CREATE OR REPLACE FUNCTION "create_game" (IN in_game_uuid uuid) RETURNS integer LANGUAGE plpgsql SECURITY DEFINER STRICT AS $$
DECLARE
  var_game_oid integer;
  var_game_rule_oid integer;
  var_field_list_oid integer;
  var_characters_list_oid integer;
BEGIN
  var_game_oid := nextval('global_id_sequence');
  var_game_rule_oid := nextval('global_id_sequence');
  var_field_list_oid := nextval('global_id_sequence');
  var_characters_list_oid := nextval('global_id_sequence');

  INSERT INTO field_list VALUES (var_field_list_oid);
  INSERT INTO characters_list VALUES (var_characters_list_oid);
  INSERT INTO game_rule VALUES (var_game_rule_oid);
  INSERT INTO game_rule_p ("game_rule_oid") VALUES (var_game_rule_oid);
  INSERT INTO game VALUES (var_game_oid);
  INSERT INTO game_p VALUES (var_game_oid,in_game_uuid,DEFAULT,var_game_rule_oid,var_field_list_oid,var_characters_list_oid);

  RETURN var_game_oid;
END
$$;