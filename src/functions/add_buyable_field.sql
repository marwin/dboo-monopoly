CREATE OR REPLACE FUNCTION "add_buyable_field" (
  IN in_game_uuid uuid,
  IN in_field_name text,
  IN in_field_type text,
  IN price integer,
  IN mortage integer
) RETURNS integer LANGUAGE plpgsql SECURITY DEFINER STRICT AS $$
DECLARE
  var_field_list_oid integer;
  var_field_oid integer;
  var_next_position integer;
BEGIN
  var_field_list_oid := (SELECT "field_list_oid" FROM "game_p" WHERE "game_id" = in_game_uuid);
  var_field_oid := nextval('global_id_sequence');
  var_next_position := (SELECT COUNT(*) FROM field_list_p WHERE "field_list_oid" = var_field_list_oid);
  
  INSERT INTO field VALUES (var_field_oid);
  INSERT INTO field_p VALUES (var_field_oid, in_field_name, in_field_type);
  INSERT INTO buyable_field VALUES (var_field_oid);
  INSERT INTO buyable_field_p VALUES (var_field_oid, NULL, price, mortage, false);
  INSERT INTO field_list_p VALUES (var_field_list_oid,var_next_position,var_field_oid,var_field_oid,NULL);
  
  return var_field_oid;
END
$$;
