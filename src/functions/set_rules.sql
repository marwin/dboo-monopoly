CREATE OR REPLACE FUNCTION "set_rules" (
  IN in_game_uuid uuid,
  IN in_startingcash integer,
  IN in_startingproperties integer,
  IN in_startingpropertyauction boolean,
  IN in_usespeeddie boolean,
  IN in_buildrules varchar(5),
  IN in_trade_properties_with_houses boolean,
  IN in_max_houses integer,
  IN in_max_hotels integer,
  IN in_houses_per_hotel integer,
  IN in_player_gets_properties_on_bankruptcy boolean,
  IN in_house_refund_percentage integer,
  IN in_mortage_interest_percentage integer,
  IN in_salary_for_passing_go integer,
  IN in_multiply_salary_for_landing_on_go integer,
  IN in_incometax integer,
  IN in_incometax_percentage integer,
  IN in_luxurytax integer,
  IN in_freeparking_payout integer,
  IN in_max_turns_in_jail integer,
  IN in_bail_amount_to_leave_jail integer,
  IN in_collect_rent_while_in_jail boolean,
  IN in_wincondition varchar(20),
  IN in_end_game_when_percentage_of_players_are_bankrupt integer,
  IN in_end_game_after_amount_of_turns integer,
  IN in_end_game_when_player_has_totalworth integer,
  IN in_utility_rent_single_multiplyer integer,
  IN in_utility_rent_double_multiplyer integer,
  IN in_railroadrent integer,
  IN in_railroadmultiplayer integer
) RETURNS integer LANGUAGE plpgsql SECURITY DEFINER STRICT AS $$
DECLARE
  var_game_rule_oid integer;
BEGIN
  var_game_rule_oid := (SELECT "game_rule_oid" FROM "game_p" WHERE "game_id" = in_game_uuid);

  UPDATE weather SET 
    game_rule_p.startingcash = in_startingcash,
    game_rule_p.startingproperties = in_startingproperties,
    game_rule_p.startingpropertyauction = in_startingpropertyauction,
    game_rule_p.usespeeddie = in_usespeeddie,
    game_rule_p.buildrules = in_buildrules,
    game_rule_p.trade_properties_with_houses = in_trade_properties_with_houses,
    game_rule_p.max_houses = in_max_houses,
    game_rule_p.max_hotels = in_max_hotels,
    game_rule_p.houses_per_hotel = in_houses_per_hotel,
    game_rule_p.player_gets_properties_on_bankruptcy = in_player_gets_properties_on_bankruptcy,
    game_rule_p.house_refund_percentage = in_house_refund_percentage,
    game_rule_p.mortage_interest_percentage = in_mortage_interest_percentage,
    game_rule_p.salary_for_passing_go = in_salary_for_passing_go,
    game_rule_p.multiply_salary_for_landing_on_go = in_multiply_salary_for_landing_on_go,
    game_rule_p.incometax = in_incometax,
    game_rule_p.incometax_percentage = in_incometax_percentage,
    game_rule_p.luxurytax = in_luxurytax,
    game_rule_p.freeparking_payout = in_freeparking_payout,
    game_rule_p.max_turns_in_jail = in_max_turns_in_jail,
    game_rule_p.bail_amount_to_leave_jail = in_bail_amount_to_leave_jail,
    game_rule_p.collect_rent_while_in_jail = in_collect_rent_while_in_jail,
    game_rule_p.wincondition = in_wincondition,
    game_rule_p.end_bankrupt_players = in_end_bankrupt_players,
    game_rule_p.end_max_turns = in_end_max_turns,
    game_rule_p.end_totalworth = in_end_totalworth,
    game_rule_p.utility_rent = in_utility_rent,
    game_rule_p.utility_rent_multiplyer = in_utility_rent_multiplyer,
    game_rule_p.railroad_rent = in_railroad_rent,
    game_rule_p.railroad_multiplayer = in_railroad_multiplayer
  WHERE game_rule_p.game_rule_oid = var_game_rule_oid;
  
  return var_game_rule_oid;
END
$$;
