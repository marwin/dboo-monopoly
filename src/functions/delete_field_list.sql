CREATE OR REPLACE FUNCTION "delete_field_list" (
  IN in_game_uuid uuid
) RETURNS integer LANGUAGE plpgsql SECURITY DEFINER STRICT AS $$
DECLARE
  var_field_list_oid integer;
  var_field_oid integer;
  var_buyable_field_oid integer;
  var_street_field_oid integer;
  var_i integer;
  var_count integer;
  var_tmp_int integer;
BEGIN
  var_count := 0;
  var_field_list_oid := (SELECT "field_list_oid" FROM "game_p" WHERE "game_id" = in_game_uuid);
  
  FOR var_field_oid, var_buyable_field_oid, var_street_field_oid IN SELECT field_oid, buyable_field_oid, street_field_oid FROM field_list_p WHERE "field_list_oid" = var_field_list_oid LOOP
    var_tmp_int := (SELECT delete_field(var_field_list_oid, var_field_oid, var_buyable_field_oid, var_street_field_oid));
    var_count := var_count + var_tmp_int;
  END loop;
  
  return var_count;
END
$$;