CREATE OR REPLACE FUNCTION "join_game" (
  IN in_game_uuid uuid
) RETURNS table (
  error boolean,
  error_description text
) LANGUAGE plpgsql SECURITY DEFINER STRICT AS $$
DECLARE
  var_player_exists boolean;
  var_game_exists boolean;
  var_player_oid integer;
  var_game_state varchar(40);
  var_game_rule_oid integer;
  var_characters_list_oid integer;
  var_character_exists boolean;
  var_default_money integer;
  var_next_character_id integer;
  var_characters_oid integer;
BEGIN
  var_player_exists := (SELECT EXISTS(SELECT 1 FROM "player_p" WHERE "username" = session_user));
  var_game_exists := (SELECT EXISTS(SELECT 1 FROM "game_p" WHERE "game_id" = in_game_uuid));
						  
  IF var_player_exists = true THEN 
    var_player_oid := (SELECT "player_oid" FROM "player_p" WHERE "username" = session_user);
  ELSE
    var_player_oid := nextval('global_id_sequence');
    INSERT INTO player VALUES (var_player_oid);
    INSERT INTO player_p VALUES (var_player_oid, session_user);
  END IF;
  
  IF var_game_exists = true THEN
    var_game_state := (SELECT "state" FROM "game_p" WHERE "game_id" = in_game_uuid);
    IF var_game_state = 'waiting_for_players' THEN
      var_game_rule_oid := (SELECT "game_rule_oid" FROM "game_p" WHERE "game_id" = in_game_uuid);
      var_characters_list_oid := (SELECT "characters_list_oid" FROM "game_p" WHERE "game_id" = in_game_uuid);
      var_character_exists := (SELECT EXISTS(SELECT 1 FROM characters_list_p,characters_p
						    WHERE characters_list_oid = var_characters_list_oid 
						    AND characters_p.characters_oid = characters_list_p.characters_oid
						    AND characters_p.player_oid = var_player_oid));
      IF var_character_exists = false THEN
	var_default_money := (SELECT "startingcash" FROM "game_rule_p" WHERE "game_rule_oid" = var_game_rule_oid);
	var_next_character_id := (SELECT COALESCE(MAX(character_id+1),0) AS character_id FROM characters_list_p WHERE "characters_list_oid" = var_characters_list_oid);
	var_characters_oid := nextval('global_id_sequence');

	INSERT INTO characters VALUES (var_characters_oid);
	INSERT INTO characters_p VALUES (var_characters_oid, var_player_oid, 0, var_default_money, 'default', 0, 0);
	INSERT INTO characters_list_p VALUES (var_characters_list_oid, var_next_character_id, CURRENT_TIMESTAMP, false, var_characters_oid);
	RETURN QUERY SELECT false, ''::TEXT;
      ELSE
	RETURN QUERY SELECT true, 'character already exists';
      END IF;
    ELSE
      RETURN QUERY SELECT true, 'game already started';
    END IF;
  ELSE
    RETURN QUERY SELECT true, 'game does not exist';
  END IF;
END
$$;
