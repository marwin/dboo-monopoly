CREATE OR REPLACE FUNCTION "finish_turn" (
  IN in_game_uuid uuid
) RETURNS table (
  error boolean,
  error_description text
) LANGUAGE plpgsql SECURITY DEFINER STRICT AS $$
DECLARE
  var_game_info RECORD;
  var_active_characters_oid integer;
  var_player_oid integer;
  var_active_player_oid integer;
BEGIN
  SELECT * FROM "game_p" WHERE "game_id" = in_game_uuid INTO var_game_info;
                        
  var_active_characters_oid := (SELECT characters_list_p.characters_oid
                        FROM characters_list_p
                        WHERE characters_list_p.characters_list_oid = var_game_info.characters_list_oid 
                        AND characters_list_p.active = true
                        ORDER BY characters_list_p.lastactiontime ASC
                        LIMIT 1);
                        
  var_player_oid := (SELECT "player_oid" FROM "player_p" WHERE "username" = session_user);
  var_active_player_oid := (SELECT  characters_p.player_oid FROM characters_p WHERE characters_p.characters_oid = var_active_characters_oid);

  IF var_player_oid = var_active_player_oid THEN
    UPDATE characters_list_p SET lastactiontime = NOW(), active = False 
    WHERE characters_list_p.characters_list_oid = var_game_info.characters_list_oid
    AND characters_list_p.characters_oid = var_active_characters_oid;
    RETURN QUERY SELECT false, ''::TEXT;
  ELSE
    RETURN QUERY SELECT true, 'you already finished your turns'::TEXT;
  END IF;
END
$$;
