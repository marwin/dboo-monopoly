CREATE OR REPLACE FUNCTION "sell_house" (
  IN in_game_uuid uuid,
  IN in_field_position integer
) RETURNS table (
  error boolean,
  error_description text
) LANGUAGE plpgsql SECURITY DEFINER STRICT AS $$
DECLARE
  var_game_info RECORD;
  var_character RECORD;
  var_field_info RECORD;
  var_rule_info RECORD;
  
  var_player_oid integer;
  var_price integer;
BEGIN
  SELECT * FROM "game_p" WHERE "game_id" = in_game_uuid INTO var_game_info;

  var_player_oid := (SELECT "player_oid" FROM "player_p" WHERE "username" = session_user);
  SELECT * FROM characters_list_p, characters_p 
           WHERE characters_list_p.characters_list_oid = var_game_info.characters_list_oid 
           AND characters_list_p.characters_oid = characters_p.characters_oid
           AND characters_p.player_oid = var_player_oid
           INTO var_character;
  
  SELECT * FROM "field_list_p" LEFT OUTER JOIN field_p ON (field_list_p.field_oid = field_p.field_oid)
                              LEFT OUTER JOIN buyable_field_p ON (field_list_p.buyable_field_oid = buyable_field_p.buyable_field_oid)
                              LEFT OUTER JOIN characters_p ON (characters_p.characters_oid = buyable_field_p.characters_oid)
                              LEFT OUTER JOIN player_p ON (player_p.player_oid = characters_p.player_oid)
                              LEFT OUTER JOIN street_field_p ON (field_list_p.street_field_oid = street_field_p.street_field_oid)
                              WHERE field_list_p.field_list_oid = var_game_info.field_list_oid
                              AND field_list_p.position = var_character.position
                              INTO var_field_info;

  SELECT * FROM game_rule_p WHERE game_rule_oid = var_game_info.game_rule_oid INTO var_rule_info;

  IF var_field_info.street_field_oid IS NOT NULL THEN
    IF var_field_info.characters_oid = var_character.characters_oid THEN
      IF var_field_info.house_count > 0 THEN
        var_price = (var_field_info.price * 2 / 10) * var_rule_info.house_refund_percentage / 100;
        UPDATE characters_p SET money = money + var_price
        WHERE characters_p.characters_oid = var_character.characters_oid;

        UPDATE street_field_p SET house_count = house_count - 1
        WHERE street_field_p.street_field_oid  = var_field_info.street_field_oid;
        RETURN QUERY SELECT false, ''::TEXT;
      ELSE
        RETURN QUERY SELECT true, 'there are no houses to sell'::TEXT;
      END IF;
    ELSE
      RETURN QUERY SELECT true, 'you do not own this street'::TEXT;
    END IF;
  ELSE
    RETURN QUERY SELECT true, 'can only buy houses on streets'::TEXT;
  END IF;
END
$$;
