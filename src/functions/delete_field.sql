CREATE OR REPLACE FUNCTION "delete_field" (
  IN in_field_list_oid integer,
  IN in_field_oid integer,
  IN in_buyable_field_oid integer,
  IN in_street_field_oid integer
) RETURNS integer LANGUAGE plpgsql AS $$
DECLARE
  var_count integer;
  var_tmp_int integer;
BEGIN
  var_count := 0;
  
  IF in_field_oid IS NOT NULL THEN 
    DELETE FROM field_list_p WHERE "field_list_oid" = in_field_list_oid AND "field_oid" = in_field_oid;
    GET DIAGNOSTICS var_tmp_int = ROW_COUNT;
    var_count := var_count + var_tmp_int;
    
    DELETE FROM field_p WHERE "field_oid" = in_field_oid;
    GET DIAGNOSTICS var_tmp_int = ROW_COUNT;
    var_count := var_count + var_tmp_int;

    DELETE FROM field WHERE "field_oid" = in_field_oid;
    GET DIAGNOSTICS var_tmp_int = ROW_COUNT;
    var_count := var_count + var_tmp_int;
  END IF;
    
  IF in_buyable_field_oid IS NOT NULL THEN 
    DELETE FROM buyable_field_p WHERE "buyable_field_oid" = in_buyable_field_oid;
    GET DIAGNOSTICS var_tmp_int = ROW_COUNT;
    var_count := var_count + var_tmp_int;
    DELETE FROM buyable_field WHERE "buyable_field_oid" = in_buyable_field_oid;
    GET DIAGNOSTICS var_tmp_int = ROW_COUNT;
    var_count := var_count + var_tmp_int;
  END IF;
  
  IF in_street_field_oid IS NOT NULL THEN 
    DELETE FROM street_field_p WHERE "street_field_oid" = in_street_field_oid;
    GET DIAGNOSTICS var_tmp_int = ROW_COUNT;
    var_count := var_count + var_tmp_int;
    DELETE FROM street_field WHERE "street_field_oid" = in_street_field_oid;
    GET DIAGNOSTICS var_tmp_int = ROW_COUNT;
    var_count := var_count + var_tmp_int;
  END IF;
  
--   GET DIAGNOSTICS var_tmp_int = ROW_COUNT;
--   var_count = var_count + var_tmp_int;
  
  return var_count;
END
$$;

CREATE OR REPLACE FUNCTION "delete_field" (
  IN in_game_uuid uuid,
  IN in_field_position integer
) RETURNS integer LANGUAGE plpgsql SECURITY DEFINER STRICT AS $$
DECLARE
  var_field_list_oid integer;
  var_field_oid integer;
  var_buyable_field_oid integer;
  var_street_field_oid integer;
  var_count integer;
BEGIN
  var_field_list_oid := (SELECT "field_list_oid" FROM "game_p" WHERE "game_id" = in_game_uuid);
  SELECT field_oid, buyable_field_oid, street_field_oid
  INTO var_field_oid, var_buyable_field_oid, var_street_field_oid
  FROM field_list_p 
  WHERE "field_list_oid" = var_field_list_oid AND "position" = in_field_position;
  
  var_count := (SELECT delete_field(var_field_list_oid, var_field_oid, var_buyable_field_oid, var_street_field_oid));

  return var_count;
END
$$;