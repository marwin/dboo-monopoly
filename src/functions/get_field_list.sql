CREATE OR REPLACE FUNCTION "get_field_list" (
  IN in_game_uuid uuid
) RETURNS table (
  pos integer, 
  field_name text, 
  field_type text, 
  price integer, 
  mortage integer, 
  mortaged boolean, 
  owner character varying(40),
  color text,
  house_count integer,
  base_rent integer, 
  single_house_rent integer, 
  double_house_rent integer, 
  triple_house_rent integer, 
  quadruple_house_rent integer, 
  hotel_rent integer
) LANGUAGE plpgsql SECURITY DEFINER STRICT AS $$
DECLARE
  var_field_list_oid integer;

BEGIN
  var_field_list_oid := (SELECT "field_list_oid" FROM "game_p" WHERE "game_id" = in_game_uuid);

  RETURN QUERY SELECT field_list_p.position, field_p.field_name, field_p.field_type, buyable_field_p.price, 
			buyable_field_p.mortage, buyable_field_p.mortaged, player_p.username, street_field_p.color, 
			street_field_p.house_count, street_field_p.base_rent, street_field_p.single_house_rent, 
			street_field_p.double_house_rent, street_field_p.triple_house_rent, 
			street_field_p.quadruple_house_rent, street_field_p.hotel_rent
    FROM "field_list_p" LEFT OUTER JOIN field_p ON (field_list_p.field_oid = field_p.field_oid)
                        LEFT OUTER JOIN buyable_field_p ON (field_list_p.buyable_field_oid = buyable_field_p.buyable_field_oid)
                        LEFT OUTER JOIN characters_p ON (characters_p.characters_oid = buyable_field_p.characters_oid)
                        LEFT OUTER JOIN player_p ON (player_p.player_oid = characters_p.player_oid)
                        LEFT OUTER JOIN street_field_p ON (field_list_p.street_field_oid = street_field_p.street_field_oid)
                        WHERE "field_list_oid" = var_field_list_oid
                        ORDER BY field_list_p.position ASC;
END
$$;