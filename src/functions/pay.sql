CREATE OR REPLACE FUNCTION "pay" (
  IN in_game_uuid uuid,
  IN in_value integer,
  IN in_recipient varchar(40)
) RETURNS table (
  error boolean,
  error_description text
) LANGUAGE plpgsql SECURITY DEFINER STRICT AS $$
DECLARE
  var_game_info RECORD;
  var_player_character RECORD;
  this RECORD;
 
  var_player_oid integer;
  var_character_count integer;
BEGIN
  SELECT * FROM "game_p" WHERE "game_id" = in_game_uuid INTO var_game_info;
  var_player_oid := (SELECT "player_oid" FROM "player_p" WHERE "username" = session_user);

  SELECT * FROM characters_list_p, characters_p 
           WHERE characters_list_p.characters_list_oid = var_game_info.characters_list_oid 
           AND characters_list_p.characters_oid = characters_p.characters_oid
           AND characters_p.player_oid = var_player_oid
           INTO var_player_character;

  IF in_recipient = 'everyone' THEN
    var_character_count = (SELECT COUNT(*) FROM characters_list_p WHERE "characters_list_oid" = var_game_info.characters_list_oid);

    IF var_player_character.money - (var_character_count - 1) * in_value >= 0 THEN
      FOR this IN SELECT * FROM characters_list_p WHERE "characters_list_oid" = var_game_info.characters_list_oid LOOP
        UPDATE characters_p SET money = money + in_value
        WHERE characters_p.characters_oid = this.characters_oid;
      END LOOP;
      
      UPDATE characters_p SET money = money - var_character_count * in_value
      WHERE characters_p.characters_oid = var_player_character.characters_oid;
      
      RETURN QUERY SELECT false, ''::TEXT;
    ELSE
      RETURN QUERY SELECT true, 'not enough money'::TEXT;
    END IF;
  ELSE
    IF var_player_character.money - in_value >= 0 THEN
      IF (SELECT EXISTS(
                          SELECT 1 FROM characters_list_p, characters_p, player_p
                          WHERE characters_list_p.characters_list_oid = var_game_info.characters_list_oid 
                          AND characters_list_p.characters_oid = characters_p.characters_oid
                          AND characters_p.player_oid = player_p.player_oid
                          AND "username" = in_recipient
                      )
        ) = true then

        UPDATE characters_p SET money = money + in_value
        WHERE characters_p.characters_oid = (SELECT characters_p.characters_oid FROM characters_list_p, characters_p, player_p
                                              WHERE characters_list_p.characters_list_oid = var_game_info.characters_list_oid 
                                              AND characters_list_p.characters_oid = characters_p.characters_oid
                                              AND characters_p.player_oid = player_p.player_oid
                                              AND "username" = in_recipient);
      END IF;

      UPDATE characters_p SET money = money - in_value
      WHERE characters_p.characters_oid = var_player_character.characters_oid;
    ELSE
      RETURN QUERY SELECT true, 'not enough money'::TEXT;
    END IF;
  END IF;
END
$$;
