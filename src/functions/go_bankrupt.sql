CREATE OR REPLACE FUNCTION "go_bankrupt" (
  IN in_game_uuid uuid,
  IN in_recipient varchar(40)
) RETURNS table (
  error boolean,
  error_description text
) LANGUAGE plpgsql SECURITY DEFINER STRICT AS $$
DECLARE
  var_game_info RECORD;
  var_player_character RECORD;
  var_recipient_character RECORD;
 
  var_player_oid integer;
  var_character_count integer;
BEGIN
  SELECT * FROM "game_p" WHERE "game_id" = in_game_uuid INTO var_game_info;
  var_player_oid := (SELECT "player_oid" FROM "player_p" WHERE "username" = session_user);

  SELECT * FROM characters_list_p, characters_p 
           WHERE characters_list_p.characters_list_oid = var_game_info.characters_list_oid 
           AND characters_list_p.characters_oid = characters_p.characters_oid
           AND characters_p.player_oid = var_player_oid
           INTO var_player_character;

  IF (SELECT EXISTS(
                      SELECT 1 FROM characters_list_p, characters_p, player_p
                      WHERE characters_list_p.characters_list_oid = var_game_info.characters_list_oid 
                      AND characters_list_p.characters_oid = characters_p.characters_oid
                      AND characters_p.player_oid = player_p.player_oid
                      AND "username" = in_recipient
                  )
    ) = true then

    
    SELECT * FROM characters_list_p, characters_p, player_p
              WHERE characters_list_p.characters_list_oid = var_game_info.characters_list_oid 
              AND characters_list_p.characters_oid = characters_p.characters_oid
              AND characters_p.player_oid = player_p.player_oid
              AND "username" = in_recipient
              INTO var_recipient_character;
    
    UPDATE buyable_field_p SET characters_oid = var_recipient_character.characters_oid
    WHERE characters_oid = var_player_character.characters_oid;
    
    UPDATE characters_p SET money = money + var_player_character.money, get_out_of_jail_free_count = get_out_of_jail_free_count + var_player_character.get_out_of_jail_free_count
    WHERE characters_oid = var_recipient_character.characters_oid;
    
  ELSE
    UPDATE buyable_field_p SET characters_oid = NULL
    WHERE characters_oid = var_player_character.characters_oid;
  END IF;

  DELETE FROM characters_list_p WHERE characters_oid = var_player_character.characters_oid;
  DELETE FROM characters_p WHERE characters_oid = var_player_character.characters_oid;
  DELETE FROM characters WHERE characters_oid = var_player_character.characters_oid;
  
  RETURN QUERY SELECT false, ''::TEXT;
END
$$;
