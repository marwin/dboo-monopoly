CREATE OR REPLACE FUNCTION "buy_field" (
  IN in_game_uuid uuid
) RETURNS table (
  error boolean,
  error_description text
) LANGUAGE plpgsql SECURITY DEFINER STRICT AS $$
DECLARE
  var_game_info RECORD;
  var_active_character RECORD;
  var_field_info RECORD;
  
  var_player_oid integer;
  var_active_player_oid integer;
BEGIN
  SELECT * FROM "game_p" WHERE "game_id" = in_game_uuid INTO var_game_info;
  SELECT * FROM characters_list_p, characters_p 
           WHERE characters_list_p.characters_list_oid = var_game_info.characters_list_oid 
           AND characters_list_p.active = true
           AND characters_list_p.characters_oid = characters_p.characters_oid
           ORDER BY characters_list_p.lastactiontime ASC
           LIMIT 1
           INTO var_active_character;
  
  var_player_oid := (SELECT "player_oid" FROM "player_p" WHERE "username" = session_user);
  var_active_player_oid := (SELECT  characters_p.player_oid FROM characters_p WHERE characters_p.characters_oid = var_active_character.characters_oid);
  
  IF var_player_oid = var_active_player_oid THEN
    SELECT * FROM "field_list_p" LEFT OUTER JOIN field_p ON (field_list_p.field_oid = field_p.field_oid)
                                LEFT OUTER JOIN buyable_field_p ON (field_list_p.buyable_field_oid = buyable_field_p.buyable_field_oid)
                                LEFT OUTER JOIN characters_p ON (characters_p.characters_oid = buyable_field_p.characters_oid)
                                LEFT OUTER JOIN player_p ON (player_p.player_oid = characters_p.player_oid)
                                LEFT OUTER JOIN street_field_p ON (field_list_p.street_field_oid = street_field_p.street_field_oid)
                                WHERE field_list_p.field_list_oid = var_game_info.field_list_oid
                                AND field_list_p.position = var_active_character.position
                                INTO var_field_info;
      
    IF var_field_info.buyable_field_oid IS NOT NULL THEN
      IF var_field_info.characters_oid IS NULL THEN
        IF var_active_character.money >= var_field_info.price THEN
          UPDATE characters_p SET money = money - var_field_info.price 
          WHERE characters_p.characters_oid = var_active_character.characters_oid;

          UPDATE buyable_field_p SET characters_oid = var_active_character.characters_oid
          WHERE buyable_field_p.buyable_field_oid  = var_field_info.buyable_field_oid;
          RETURN QUERY SELECT false, ''::TEXT;
        ELSE
          RETURN QUERY SELECT true, 'not enough money'::TEXT;
        END IF;
      ELSE
        RETURN QUERY SELECT true, 'field already owned'::TEXT;
      END IF;
    ELSE
      RETURN QUERY SELECT true, 'cannot buy unbuyable field'::TEXT;
    END IF;
  ELSE
    RETURN QUERY SELECT true, 'not your turn'::TEXT;
  END IF;
END
$$;
