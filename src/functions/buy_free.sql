CREATE OR REPLACE FUNCTION "buy_free" (
  IN in_game_uuid uuid
) RETURNS table (
  error boolean,
  error_description text
) LANGUAGE plpgsql SECURITY DEFINER STRICT AS $$
DECLARE
  var_game_info RECORD;
  var_rule_info RECORD;
  var_player_character RECORD;
  this RECORD;
 
  var_player_oid integer;
  var_character_count integer;
BEGIN
  SELECT * FROM "game_p" WHERE "game_id" = in_game_uuid INTO var_game_info;
  SELECT * FROM "game_rule_p" WHERE "game_rule_oid" = var_game_info.game_rule_oid INTO var_rule_info;
  var_player_oid := (SELECT "player_oid" FROM "player_p" WHERE "username" = session_user);

  SELECT * FROM characters_list_p, characters_p 
           WHERE characters_list_p.characters_list_oid = var_game_info.characters_list_oid 
           AND characters_list_p.characters_oid = characters_p.characters_oid
           AND characters_p.player_oid = var_player_oid
           INTO var_player_character;

  IF var_player_character.money - var_rule_info.bail_amount_to_leave_jail >= 0 THEN
    UPDATE characters_p SET money = money - var_rule_info.bail_amount_to_leave_jail, state = 'default'::varchar(40)
    WHERE characters_p.characters_oid = var_player_character.characters_oid;
  ELSE
    RETURN QUERY SELECT true, 'not enough money'::TEXT;
  END IF;

END
$$;
