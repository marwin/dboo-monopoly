CREATE OR REPLACE FUNCTION "roll_dice" (
  IN in_game_uuid uuid
) RETURNS table (
  dice_eye_count_one integer,
  dice_eye_count_two integer,
  start_pos integer,
  end_pos integer,
  action text, 
  action_value integer,
  action_target varchar(40),
  action_description text
) LANGUAGE plpgsql SECURITY DEFINER STRICT AS $$
DECLARE
  var_game_info RECORD;
  var_field_info RECORD;
  var_rule_info RECORD;
  var_card RECORD;

  var_active_characters_oid integer;
  var_player_oid integer;
  var_active_player_oid integer;
  var_dice_eye_count_one integer;
  var_dice_eye_count_two integer;
  var_dice_eye_count integer;
  var_field_count integer;
  var_current_position integer;
  var_new_current_position integer;
  var_money_amount integer;
  var_tmp_count integer;
  var_recipient varchar(40);
BEGIN
  SELECT * FROM "game_p" WHERE "game_id" = in_game_uuid INTO var_game_info;

--   var_active_characters_oid := (SELECT characters_list_p.characters_oid
--                         FROM characters_list_p
--                         WHERE characters_list_p.characters_list_oid = var_game_info.characters_list_oid 
--                         ORDER BY characters_list_p.active DESC, characters_list_p.lastactiontime ASC
--                         LIMIT 1);
                        
  var_active_characters_oid := (SELECT characters_list_p.characters_oid
                        FROM characters_list_p
                        WHERE characters_list_p.characters_list_oid = var_game_info.characters_list_oid 
                        AND characters_list_p.active = false
                        ORDER BY characters_list_p.lastactiontime ASC
                        LIMIT 1);
                        
  var_player_oid := (SELECT "player_oid" FROM "player_p" WHERE "username" = session_user);
  var_active_player_oid := (SELECT  characters_p.player_oid FROM characters_p WHERE characters_p.characters_oid = var_active_characters_oid);

  -- if player owns active character
  IF var_player_oid = var_active_player_oid THEN
    var_dice_eye_count_one := (SELECT (5 * random() + 1)::int);
    var_dice_eye_count_two := (SELECT (5 * random() + 1)::int);
    var_dice_eye_count := var_dice_eye_count_one + var_dice_eye_count_two;
    
    var_field_count := (SELECT COUNT(*) FROM field_list_p WHERE "field_list_oid" = var_game_info.field_list_oid);
    var_current_position := (SELECT characters_p.position FROM characters_p WHERE characters_p.characters_oid = var_active_characters_oid);
    var_new_current_position := (var_current_position + var_dice_eye_count) % var_field_count;
    
    -- only update character_list if not a double
     IF var_dice_eye_count_one != var_dice_eye_count_two THEN
      UPDATE characters_list_p SET lastactiontime = NOW(), active = True 
      WHERE characters_list_p.characters_list_oid = var_game_info.characters_list_oid
      AND characters_list_p.characters_oid = var_active_characters_oid;
     END IF;

    IF (SELECT state FROM characters_p WHERE characters_p.characters_oid = var_active_characters_oid) = 'inJail' THEN
      UPDATE characters_list_p SET lastactiontime = NOW(), active = False 
      WHERE characters_list_p.characters_list_oid = var_game_info.characters_list_oid
      AND characters_list_p.characters_oid = var_active_characters_oid;
      IF var_dice_eye_count_one != var_dice_eye_count_two THEN
        RETURN QUERY SELECT var_dice_eye_count_one, var_dice_eye_count_two, var_current_position, var_current_position, 'still_not_free'::TEXT, 0, ''::varchar(40), 'You did not get Free. :/'::TEXT;
      ELSE
        UPDATE characters_p SET state = 'default'::varchar(40) WHERE characters_p.characters_oid = var_active_characters_oid;
        RETURN QUERY SELECT var_dice_eye_count_one, var_dice_eye_count_two, var_current_position, var_current_position, 'you_got_free'::TEXT, 0, ''::varchar(40), 'You got Free! :D'::TEXT;
      END IF;
    ELSE
      UPDATE characters_p SET position = var_new_current_position WHERE characters_p.characters_oid = var_active_characters_oid;
      -- var_field_count := (SELECT COUNT(*) FROM field_list_p WHERE "field_list_oid" = var_game_info.field_list_oid 
      --                                                          AND field_list_p.position = var_new_current_position);
      -- SELECT * from get_field_list(in_game_uuid) WHERE pos = var_new_current_position INTO var_field_info;
      SELECT * FROM "field_list_p" LEFT OUTER JOIN field_p ON (field_list_p.field_oid = field_p.field_oid)
                          LEFT OUTER JOIN buyable_field_p ON (field_list_p.buyable_field_oid = buyable_field_p.buyable_field_oid)
                          LEFT OUTER JOIN characters_p ON (characters_p.characters_oid = buyable_field_p.characters_oid)
                          LEFT OUTER JOIN player_p ON (player_p.player_oid = characters_p.player_oid)
                          LEFT OUTER JOIN street_field_p ON (field_list_p.street_field_oid = street_field_p.street_field_oid)
                          WHERE field_list_p.field_list_oid = var_game_info.field_list_oid
                          AND field_list_p.position = var_new_current_position
                          INTO var_field_info;
      SELECT * FROM game_rule_p WHERE game_rule_oid = var_game_info.game_rule_oid INTO var_rule_info;
      
      
      IF var_field_info.field_type != 'Go' AND var_current_position > var_new_current_position THEN
        UPDATE characters_p SET money = money + var_rule_info.salary_for_passing_go
          WHERE characters_p.characters_oid = var_active_characters_oid;
      END IF;
    
      -- if is buyable
      IF var_field_info.buyable_field_oid IS NOT NULL THEN
        -- if is owned
        IF var_field_info.characters_oid IS NOT NULL THEN
          -- if is owned by someone else
          IF var_field_info.characters_oid != var_active_characters_oid THEN
            var_money_amount := 0;
            IF var_field_info.street_field_oid IS NOT NULL THEN
              CASE var_field_info.house_count
                WHEN 0 AND var_field_info.house_count < var_rule_info.houses_per_hotel THEN
                  var_money_amount := var_field_info.base_rent;
                WHEN 1 AND var_field_info.house_count < var_rule_info.houses_per_hotel THEN
                  var_money_amount := var_field_info.single_house_rent;
                WHEN 2 AND var_field_info.house_count < var_rule_info.houses_per_hotel THEN
                  var_money_amount := var_field_info.double_house_rent;
                WHEN 3 AND var_field_info.house_count < var_rule_info.houses_per_hotel THEN
                  var_money_amount := var_field_info.triple_house_rent;
                WHEN 4 AND var_field_info.house_count < var_rule_info.houses_per_hotel THEN
                  var_money_amount := var_field_info.quadruple_house_rent;
                ELSE
                  var_money_amount := var_field_info.hotel_rent;
              END CASE;
            ELSE
              CASE var_field_info.field_type
                WHEN 'Railroad' THEN
                  var_tmp_count := (SELECT COUNT(*) 
                    FROM "field_list_p" LEFT OUTER JOIN field_p ON (field_list_p.field_oid = field_p.field_oid)
                    LEFT OUTER JOIN buyable_field_p ON (field_list_p.buyable_field_oid = buyable_field_p.buyable_field_oid)
                    WHERE "field_list_oid" = var_game_info.field_list_oid
                    AND "characters_oid" = var_field_info.characters_oid
                    AND "field_type" = 'Railroad');
                  var_money_amount := var_rule_info.railroad_rent * pow(var_rule_info.railroad_multiplayer,var_tmp_count-1);
            
                WHEN 'Utility' THEN
                  var_tmp_count := (SELECT COUNT(*) 
                    FROM "field_list_p" LEFT OUTER JOIN field_p ON (field_list_p.field_oid = field_p.field_oid)
                    LEFT OUTER JOIN buyable_field_p ON (field_list_p.buyable_field_oid = buyable_field_p.buyable_field_oid)
                    WHERE "field_list_oid" = var_game_info.field_list_oid
                    AND "characters_oid" = var_field_info.characters_oid
                    AND "field_type" = 'Utility');
                  var_money_amount := var_rule_info.utility_rent * pow(var_rule_info.utility_rent_multiplyer,var_tmp_count-1);
                ELSE
                  RETURN QUERY SELECT 0, 0, 0, 0, 'no_such_type_1'::TEXT, 0, ''::varchar(40), var_field_info.field_type;
              END CASE;
            END IF;
            
            var_recipient := (SELECT player_p.username FROM characters_p,player_p
                            WHERE characters_p.characters_oid = var_field_info.characters_oid
                            AND player_p.player_oid = characters_p.player_oid);

            RETURN QUERY SELECT var_dice_eye_count_one, var_dice_eye_count_two, var_current_position, var_new_current_position, 'pay'::TEXT, var_money_amount, var_recipient, ''::TEXT;
          END IF;
        ELSE
          -- buy field
          RETURN QUERY SELECT var_dice_eye_count_one, var_dice_eye_count_two, var_current_position, var_new_current_position, 'buy'::TEXT, var_field_info.price, ''::varchar(40), ''::TEXT;
        END IF;
      ELSE
        CASE var_field_info.field_type
            WHEN 'Chance' THEN
              SELECT * FROM cards_p WHERE chance_stack = true ORDER BY RANDOM() LIMIT 1 INTO var_card;
              RETURN QUERY SELECT var_dice_eye_count_one, var_dice_eye_count_two, var_current_position, var_new_current_position, var_card.action, var_card.actionvalue, ''::varchar(40), var_card.description;
            WHEN 'CommunityChest' THEN
              SELECT * FROM cards_p WHERE chance_stack = false ORDER BY RANDOM() LIMIT 1 INTO var_card;
              RETURN QUERY SELECT var_dice_eye_count_one, var_dice_eye_count_two, var_current_position, var_new_current_position, var_card.action, var_card.actionvalue, ''::varchar(40), var_card.description;
            WHEN 'FreeParking' THEN
              UPDATE characters_p SET money = money + var_rule_info.freeparking_payout WHERE characters_p.characters_oid = var_active_characters_oid;
              RETURN QUERY SELECT var_dice_eye_count_one, var_dice_eye_count_two, var_current_position, var_new_current_position, 'DISPLAY'::TEXT, var_rule_info.freeparking_payout, ''::varchar(40), var_field_info.field_type;
            WHEN 'Go' THEN
              UPDATE characters_p SET money = money + (var_rule_info.salary_for_passing_go * var_rule_info.multiply_salary_for_landing_on_go) WHERE characters_p.characters_oid = var_active_characters_oid;
              RETURN QUERY SELECT var_dice_eye_count_one, var_dice_eye_count_two, var_current_position, var_new_current_position, 'DISPLAY'::TEXT, var_rule_info.salary_for_passing_go * var_rule_info.multiply_salary_for_landing_on_go, ''::varchar(40), var_field_info.field_type;
            WHEN 'GoToJail' THEN
              var_new_current_position := (SELECT pos from get_field_list(in_game_uuid) WHERE field_type = 'Jail' LIMIT 1);
              UPDATE characters_p SET position = var_new_current_position, state = 'inJail'::varchar(40) WHERE characters_p.characters_oid = var_active_characters_oid;
              RETURN QUERY SELECT var_dice_eye_count_one, var_dice_eye_count_two, var_current_position, var_new_current_position, 'DISPLAY'::TEXT, 0, ''::varchar(40), var_field_info.field_type;
            WHEN 'IncomeTax' THEN
              RETURN QUERY SELECT var_dice_eye_count_one, var_dice_eye_count_two, var_current_position, var_new_current_position, 'pay'::TEXT, var_rule_info.incometax, ''::varchar(40), var_field_info.field_type;
            WHEN 'Jail' THEN
              RETURN QUERY SELECT var_dice_eye_count_one, var_dice_eye_count_two, var_current_position, var_new_current_position, 'DISPLAY'::TEXT, 0, ''::varchar(40), var_field_info.field_type;
            WHEN 'LuxuryTax' THEN
              RETURN QUERY SELECT var_dice_eye_count_one, var_dice_eye_count_two, var_current_position, var_new_current_position, 'pay'::TEXT, var_rule_info.luxurytax, ''::varchar(40), var_field_info.field_type;
            ELSE
              RETURN QUERY SELECT 0, 0, 0, 0, 'no_such_type_2'::TEXT, 0, ''::varchar(40), var_field_info.field_type;
        END CASE;
      END IF;
    END IF;
  ELSE
    RETURN QUERY SELECT 0, 0, 0, 0, 'not_your_turn'::TEXT, 0, ''::varchar(40), ''::TEXT;
    -- return not your turn
  END IF;
END
$$;
