CREATE OR REPLACE FUNCTION "get_character_list" (
  IN in_game_uuid uuid
) RETURNS table (
  username varchar(40),
  character_id integer, 
  lastactiontime timestamp, 
  pos integer, 
  money integer, 
  state varchar(40), 
  state_rounds integer,
  get_out_of_jail_free_count integer
) LANGUAGE plpgsql SECURITY DEFINER STRICT AS $$
DECLARE
  var_characters_list_oid integer;
BEGIN
  var_characters_list_oid := (SELECT "characters_list_oid" FROM "game_p" WHERE "game_id" = in_game_uuid);

  RETURN QUERY SELECT player_p.username, characters_list_p.character_id, characters_list_p.lastactiontime,
			characters_p.position, characters_p.money, characters_p.state,
			characters_p.state_rounds, characters_p.get_out_of_jail_free_count
			FROM characters_list_p,characters_p,player_p
			WHERE characters_list_p.characters_list_oid = var_characters_list_oid 
			AND characters_p.characters_oid = characters_list_p.characters_oid
			AND player_p.player_oid = characters_p.player_oid
                        ORDER BY characters_list_p.active DESC, characters_list_p.lastactiontime ASC;
END
$$;