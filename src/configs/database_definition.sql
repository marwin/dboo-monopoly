CREATE SEQUENCE global_id_sequence
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
ALTER SEQUENCE global_id_sequence OWNER TO monopoly;


DROP TABLE IF EXISTS card_p;
DROP TABLE IF EXISTS card;

CREATE TABLE card (card_oid integer PRIMARY KEY DEFAULT nextval('global_id_sequence'));
ALTER TABLE card OWNER TO monopoly;

CREATE TABLE card_p (
    card_oid integer REFERENCES card,
    is_community_chest boolean NOT NULL, 
    description text NOT NULL,
    actiontype text NOT NULL,
    actionvalue integer NOT NULL,
    PRIMARY KEY (card_oid)
);
ALTER TABLE card_p OWNER TO monopoly;

DROP TABLE IF EXISTS board_p;
DROP TABLE IF EXISTS board;
DROP TABLE IF EXISTS user_owns_field;
DROP TABLE IF EXISTS field_street_data;
DROP TABLE IF EXISTS street_data_p;
DROP TABLE IF EXISTS street_data;
DROP TABLE IF EXISTS field_buyable_data;
DROP TABLE IF EXISTS buyable_data_p;
DROP TABLE IF EXISTS buyable_data;
DROP TABLE IF EXISTS field_list_p;
DROP TABLE IF EXISTS field_list;
DROP TABLE IF EXISTS field_p;
DROP TABLE IF EXISTS field;
DROP TABLE IF EXISTS board_rule_p;
DROP TABLE IF EXISTS board_rule;
DROP TABLE IF EXISTS item_p;
DROP TABLE IF EXISTS item;
DROP TABLE IF EXISTS character_list_p;
DROP TABLE IF EXISTS character_list;
DROP TABLE IF EXISTS character_p;
DROP TABLE IF EXISTS character;
DROP TABLE IF EXISTS player_p;
DROP TABLE IF EXISTS player;

CREATE TABLE player (player_oid integer PRIMARY KEY DEFAULT nextval('global_id_sequence'));
ALTER TABLE player OWNER TO monopoly;

CREATE TABLE player_p (
    player_oid integer REFERENCES player,
    username varchar(40) NOT NULL,
    PRIMARY KEY (player_oid)
);
ALTER TABLE player_p OWNER TO monopoly;

CREATE TABLE character (character_oid integer PRIMARY KEY DEFAULT nextval('global_id_sequence'));
ALTER TABLE character OWNER TO monopoly;

CREATE TABLE character_p (
    character_oid integer REFERENCES character,
    figure varchar(40) NOT NULL,
    position integer DEFAULT 0,
    money integer DEFAULT 0,
    state varchar(40) DEFAULT 'free',
    state_rounds integer DEFAULT 0,
    lastactiontime timestamp DEFAULT CURRENT_TIMESTAMP, 
    PRIMARY KEY (character_oid)
);
ALTER TABLE character_p OWNER TO monopoly;

CREATE TABLE character_item (
    character_oid integer REFERENCES character,
    card_oid integer REFERENCES card,
    PRIMARY KEY (item_list_oid)
);
ALTER TABLE character_item OWNER TO monopoly;

CREATE TABLE character_list (character_list_oid integer PRIMARY KEY DEFAULT nextval('global_id_sequence'));
ALTER TABLE character_list OWNER TO monopoly;

CREATE TABLE character_list_p (
    character_list_oid integer REFERENCES character_list,
    character_oid integer REFERENCES character,
    PRIMARY KEY (character_list_oid)
);
ALTER TABLE character_list_p OWNER TO monopoly;

CREATE TABLE board_rule (board_rule_oid integer PRIMARY KEY DEFAULT nextval('global_id_sequence'));
ALTER TABLE board_rule OWNER TO monopoly;

CREATE TABLE board_rule_p (
    board_rule_oid integer REFERENCES board_rule,
    board_rule_name varchar(40) NOT NULL DEFAULT 'custom',
    startingcash integer DEFAULT 1500,
    startingproperties integer DEFAULT 0,
    startingpropertyauction boolean DEFAULT false,
    usespeeddie boolean DEFAULT false,
    buildrules varchar(5) NOT NULL DEFAULT 'even',
    trade_properties_with_houses boolean DEFAULT false,
    max_houses integer DEFAULT 32,
    max_hotels integer DEFAULT 12,
    houses_per_hotel integer DEFAULT 4,
    player_gets_properties_on_bankruptcy boolean DEFAULT true,
    house_refund_percentage integer DEFAULT 50,
    mortage_interest_percentage integer DEFAULT 10,
    salary_for_passing_go integer DEFAULT 200,
    multiply_salary_for_landing_on_go integer DEFAULT 2,
    incometax integer DEFAULT 200,
    incometax_percentage integer DEFAULT 10,
    luxurytax integer DEFAULT 100,
    freeparking_payout integer DEFAULT 100,
    max_turns_in_jail integer DEFAULT 4,
    bail_amount_to_leave_jail integer DEFAULT 50,
    collect_rent_while_in_jail boolean DEFAULT true,
    wincondition varchar(20) NOT NULL DEFAULT 'LastPlayerLeft',
    end_game_when_percentage_of_players_are_bankrupt integer DEFAULT 0,
    end_game_after_amount_of_turns integer DEFAULT 0,
    end_game_when_player_has_totalworth integer DEFAULT 0,
    utility_rent_single_multiplyer integer DEFAULT 4,
    utility_rent_double_multiplyer integer DEFAULT 10,
    railroadrent integer DEFAULT 25,
    railroadmultiplayer integer DEFAULT 2,
    PRIMARY KEY (board_rule_oid)
);
ALTER TABLE board_rule_p OWNER TO monopoly;

CREATE TABLE field (field_oid integer PRIMARY KEY DEFAULT nextval('global_id_sequence'));
ALTER TABLE field OWNER TO monopoly;

CREATE TABLE field_p (
    field_oid integer REFERENCES field,
    field_name text NOT NULL,
    field_type text NOT NULL,
    field_position integer DEFAULT 0,
    PRIMARY KEY (field_oid)
);
ALTER TABLE field_p OWNER TO monopoly;

CREATE TABLE field_list (field_list_oid integer PRIMARY KEY DEFAULT nextval('global_id_sequence'));
ALTER TABLE field_list OWNER TO monopoly;

CREATE TABLE field_list_p (
    field_list_oid integer REFERENCES field_list,
    field_oid integer REFERENCES field,
    PRIMARY KEY (field_list_oid)
);
ALTER TABLE field_list_p OWNER TO monopoly;

CREATE TABLE buyable_data (buyable_data_oid integer PRIMARY KEY DEFAULT nextval('global_id_sequence'));
ALTER TABLE buyable_data OWNER TO monopoly;

CREATE TABLE buyable_data_p (
    buyable_data_oid integer REFERENCES buyable_data,
    price integer NOT NULL,
    mortage integer NOT NULL,
    mortaged boolean NOT NULL, 
    PRIMARY KEY (buyable_data_oid)
);
ALTER TABLE buyable_data_p OWNER TO monopoly;

CREATE TABLE field_buyable_data (
    field_oid integer REFERENCES field,
    buyable_data_oid integer REFERENCES buyable_data,
    PRIMARY KEY (field_oid,buyable_data_oid)
);
ALTER TABLE field_buyable_data OWNER TO monopoly;




CREATE TABLE street_data (street_data_oid integer PRIMARY KEY DEFAULT nextval('global_id_sequence'));
ALTER TABLE street_data OWNER TO monopoly;

CREATE TABLE street_data_p (
    street_data_oid integer REFERENCES street_data,
    color text NOT NULL,
    base_rent integer NOT NULL,
    one_house_rent integer NOT NULL,
    double_house_rent integer NOT NULL,
    triple_house_rent boolean NOT NULL,
    quadruple_house_rent boolean NOT NULL,
    hotel_rent boolean NOT NULL,
    PRIMARY KEY (street_data_oid)
);
ALTER TABLE street_data_p OWNER TO monopoly;

CREATE TABLE field_street_data (
    field_oid integer REFERENCES field,
    street_data_oid integer REFERENCES street_data,
    PRIMARY KEY (field_oid,street_data_oid)
);
ALTER TABLE field_street_data OWNER TO monopoly;



CREATE TABLE board (board_oid integer PRIMARY KEY DEFAULT nextval('global_id_sequence'));
ALTER TABLE board OWNER TO monopoly;

CREATE TABLE board_p (
    board_oid integer REFERENCES board,
    character_list_oid integer REFERENCES character_list,
    field_list_oid integer REFERENCES field_list,
    board_rule_oid integer REFERENCES board_rule,
    boardname text NOT NULL,
    starttime timestamp DEFAULT CURRENT_TIMESTAMP + INTERVAL '2 minutes',
    PRIMARY KEY (board_oid)
);
ALTER TABLE board_p OWNER TO monopoly;

CREATE TABLE player_character_list (
    player_oid integer REFERENCES player,
    character_oid integer REFERENCES character,
    PRIMARY KEY (player_oid,character_oid)
);
ALTER TABLE player_character_list OWNER TO monopoly;

CREATE TABLE character_owns_field (
    character_oid integer REFERENCES character,
    field_oid integer REFERENCES field,
    PRIMARY KEY (character_oid,field_oid)
);
ALTER TABLE character_owns_field OWNER TO monopoly;



SELECT nextval('global_id_sequence');