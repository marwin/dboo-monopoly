-- C-Relationen
DROP TABLE IF EXISTS game_p;
DROP TABLE IF EXISTS field_list_p;
DROP TABLE IF EXISTS characters_list_p;

-- P-Relationen
DROP TABLE IF EXISTS street_field_p;
DROP TABLE IF EXISTS buyable_field_p;
DROP TABLE IF EXISTS field_p;
DROP TABLE IF EXISTS characters_p;
DROP TABLE IF EXISTS game_rule_p;
DROP TABLE IF EXISTS cards_p;
DROP TABLE IF EXISTS player_p;

-- E-Relationen
DROP TABLE IF EXISTS characters_list;
DROP TABLE IF EXISTS field_list;
DROP TABLE IF EXISTS game;
DROP TABLE IF EXISTS street_field;
DROP TABLE IF EXISTS buyable_field;
DROP TABLE IF EXISTS field;
DROP TABLE IF EXISTS characters;
DROP TABLE IF EXISTS game_rule;
DROP TABLE IF EXISTS cards;
DROP TABLE IF EXISTS player;

-- E-Relationen
CREATE TABLE player (player_oid integer PRIMARY KEY DEFAULT nextval('global_id_sequence'));
ALTER TABLE player OWNER TO monopoly;

CREATE TABLE cards (cards_oid integer PRIMARY KEY DEFAULT nextval('global_id_sequence'));
ALTER TABLE cards OWNER TO monopoly;

CREATE TABLE game_rule (game_rule_oid integer PRIMARY KEY DEFAULT nextval('global_id_sequence'));
ALTER TABLE game_rule OWNER TO monopoly;

CREATE TABLE characters (characters_oid integer PRIMARY KEY DEFAULT nextval('global_id_sequence'));
ALTER TABLE characters OWNER TO monopoly;

CREATE TABLE field (field_oid integer PRIMARY KEY DEFAULT nextval('global_id_sequence'));
ALTER TABLE field OWNER TO monopoly;

CREATE TABLE buyable_field (buyable_field_oid integer PRIMARY KEY DEFAULT nextval('global_id_sequence'));
ALTER TABLE buyable_field OWNER TO monopoly;

CREATE TABLE street_field (street_field_oid integer PRIMARY KEY DEFAULT nextval('global_id_sequence'));
ALTER TABLE street_field OWNER TO monopoly;

CREATE TABLE game (game_oid integer PRIMARY KEY DEFAULT nextval('global_id_sequence'));
ALTER TABLE game OWNER TO monopoly;

CREATE TABLE field_list (field_list_oid integer PRIMARY KEY DEFAULT nextval('global_id_sequence'));
ALTER TABLE field_list OWNER TO monopoly;

CREATE TABLE characters_list (characters_list_oid integer PRIMARY KEY DEFAULT nextval('global_id_sequence'));
ALTER TABLE characters_list OWNER TO monopoly;

-- P-Relationen
CREATE TABLE player_p (
    player_oid integer REFERENCES player,
    username varchar(40) NOT NULL,
    PRIMARY KEY (player_oid)
);
ALTER TABLE player_p OWNER TO monopoly;

CREATE TABLE cards_p (
    cards_oid integer REFERENCES cards,
    chance_stack boolean NOT NULL, 
    description text NOT NULL,
    action text NOT NULL,
    actionvalue integer NOT NULL,
    PRIMARY KEY (cards_oid)
);
ALTER TABLE cards_p OWNER TO monopoly;

CREATE TABLE game_rule_p (
    game_rule_oid integer REFERENCES game_rule,
    startingcash integer DEFAULT 1500,
    startingproperties integer DEFAULT 0,
    startingpropertyauction boolean DEFAULT false,
    usespeeddie boolean DEFAULT false,
    buildrules varchar(5) NOT NULL DEFAULT 'even',
    trade_properties_with_houses boolean DEFAULT false,
    max_houses integer DEFAULT 32,
    max_hotels integer DEFAULT 12,
    houses_per_hotel integer DEFAULT 4,
    player_gets_properties_on_bankruptcy boolean DEFAULT true,
    house_refund_percentage integer DEFAULT 50,
    mortage_interest_percentage integer DEFAULT 10,
    salary_for_passing_go integer DEFAULT 200,
    multiply_salary_for_landing_on_go integer DEFAULT 2,
    incometax integer DEFAULT 200,
    incometax_percentage integer DEFAULT 10,
    luxurytax integer DEFAULT 100,
    freeparking_payout integer DEFAULT 100,
    max_turns_in_jail integer DEFAULT 4,
    bail_amount_to_leave_jail integer DEFAULT 50,
    collect_rent_while_in_jail boolean DEFAULT true,
    wincondition varchar(20) NOT NULL DEFAULT 'LastPlayerLeft',
    end_bankrupt_players integer DEFAULT 0,
    end_max_turns integer DEFAULT 0,
    end_totalworth integer DEFAULT 0,
    utility_rent integer DEFAULT 4,
    utility_rent_multiplyer integer DEFAULT 2.4,
    railroad_rent integer DEFAULT 25,
    railroad_multiplayer integer DEFAULT 2,
    PRIMARY KEY (game_rule_oid)
);
ALTER TABLE game_rule_p OWNER TO monopoly;

CREATE TABLE characters_p (
    characters_oid integer REFERENCES characters,
    player_oid integer REFERENCES player,
    position integer DEFAULT 0,
    money integer DEFAULT 0,
    state varchar(40) DEFAULT 'default',
    state_rounds integer DEFAULT 0,
    get_out_of_jail_free_count integer DEFAULT 0, 
    PRIMARY KEY (characters_oid)
);
ALTER TABLE characters_p OWNER TO monopoly;


CREATE TABLE field_p (
    field_oid integer REFERENCES field,
    field_name text NOT NULL,
    field_type text NOT NULL,
    PRIMARY KEY (field_oid)
);
ALTER TABLE field_p OWNER TO monopoly;

CREATE TABLE buyable_field_p (
    buyable_field_oid integer REFERENCES buyable_field,
    characters_oid integer REFERENCES characters,
    price integer NOT NULL,
    mortage integer NOT NULL,
    mortaged boolean NOT NULL, 
    PRIMARY KEY (buyable_field_oid)
);
ALTER TABLE buyable_field_p OWNER TO monopoly;

CREATE TABLE street_field_p (
    street_field_oid integer REFERENCES street_field,
    color text NOT NULL,
    house_count integer DEFAULT 0,
    base_rent integer NOT NULL,
    single_house_rent integer NOT NULL,
    double_house_rent integer NOT NULL,
    triple_house_rent integer NOT NULL,
    quadruple_house_rent integer NOT NULL,
    hotel_rent integer NOT NULL,
    PRIMARY KEY (street_field_oid)
);
ALTER TABLE street_field_p OWNER TO monopoly;


-- Characteristic Graph Relationen
CREATE TABLE game_p (
    game_oid integer REFERENCES game,
    game_id uuid UNIQUE NOT NULL,
    state varchar(40) DEFAULT 'waiting_for_players',
    game_rule_oid integer REFERENCES game_rule,
    field_list_oid integer REFERENCES field_list,
    characters_list_oid integer REFERENCES characters_list,
    PRIMARY KEY (game_oid)
);
ALTER TABLE game_p OWNER TO monopoly;

CREATE TABLE characters_list_p (
    characters_list_oid integer REFERENCES characters_list,
    character_id integer NOT NULL,
    lastactiontime timestamp DEFAULT CURRENT_TIMESTAMP, 
    active boolean DEFAULT false,
    characters_oid integer REFERENCES characters,
    PRIMARY KEY (characters_list_oid,character_id)
);
ALTER TABLE characters_list_p OWNER TO monopoly;

CREATE TABLE field_list_p (
    field_list_oid integer REFERENCES field_list,
    position integer NOT NULL,
    field_oid integer REFERENCES field,
    buyable_field_oid integer REFERENCES buyable_field,
    street_field_oid integer REFERENCES street_field,
    PRIMARY KEY (field_list_oid,position)
);
ALTER TABLE field_list_p OWNER TO monopoly;