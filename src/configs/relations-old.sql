-- C-Relationen
DROP TABLE IF EXISTS street_field;
DROP TABLE IF EXISTS buyable_field;
DROP TABLE IF EXISTS field;
DROP TABLE IF EXISTS field_list;
DROP TABLE IF EXISTS characters;
DROP TABLE IF EXISTS characters_list;
DROP TABLE IF EXISTS game_rule;

-- P-Relationen
DROP TABLE IF EXISTS game_p;
DROP TABLE IF EXISTS figures_p;
DROP TABLE IF EXISTS cards_p;
DROP TABLE IF EXISTS player_p;

-- E-Relationen
DROP TABLE IF EXISTS game;
DROP TABLE IF EXISTS figures;
DROP TABLE IF EXISTS cards;
DROP TABLE IF EXISTS player;

-- E-Relationen
CREATE TABLE player (player_oid integer PRIMARY KEY DEFAULT nextval('global_id_sequence'));
ALTER TABLE player OWNER TO monopoly;

CREATE TABLE cards (cards_oid integer PRIMARY KEY DEFAULT nextval('global_id_sequence'));
ALTER TABLE cards OWNER TO monopoly;

CREATE TABLE figures (figures_oid integer PRIMARY KEY DEFAULT nextval('global_id_sequence'));
ALTER TABLE figures OWNER TO monopoly;

CREATE TABLE game (game_oid integer PRIMARY KEY DEFAULT nextval('global_id_sequence'));
ALTER TABLE game OWNER TO monopoly;

-- P-Relationen
CREATE TABLE player_p (
    player_oid integer REFERENCES player,
    username varchar(40) NOT NULL,
    PRIMARY KEY (player_oid)
);
ALTER TABLE player_p OWNER TO monopoly;

CREATE TABLE cards_p (
    cards_oid integer REFERENCES cards,
    chance_stack boolean NOT NULL, 
    description text NOT NULL,
    action text NOT NULL,
    actionvalue integer NOT NULL,
    PRIMARY KEY (cards_oid)
);
ALTER TABLE cards_p OWNER TO monopoly;

CREATE TABLE figures_p (
    figures_oid integer REFERENCES figures,
    name text NOT NULL,
    image text NOT NULL,
    PRIMARY KEY (figures_oid)
);
ALTER TABLE figures_p OWNER TO monopoly;

CREATE TABLE game_p (
    game_oid integer REFERENCES game,
    game_id uuid NOT NULL,
    state varchar(40) DEFAULT 'waiting_for_players',
    game_rule_oid integer DEFAULT nextval('global_id_sequence'),
    field_list_oid integer DEFAULT nextval('global_id_sequence'),
    characters_list_oid integer DEFAULT nextval('global_id_sequence'),
    PRIMARY KEY (game_oid)
);
ALTER TABLE game_p OWNER TO monopoly;

-- Characteristic Graph Relationen
CREATE TABLE game_rule (
    game_rule_oid integer REFERENCES game_p,
    startingcash integer DEFAULT 1500,
    startingproperties integer DEFAULT 0,
    startingpropertyauction boolean DEFAULT false,
    usespeeddie boolean DEFAULT false,
    buildrules varchar(5) NOT NULL DEFAULT 'even',
    trade_properties_with_houses boolean DEFAULT false,
    max_houses integer DEFAULT 32,
    max_hotels integer DEFAULT 12,
    houses_per_hotel integer DEFAULT 4,
    player_gets_properties_on_bankruptcy boolean DEFAULT true,
    house_refund_percentage integer DEFAULT 50,
    mortage_interest_percentage integer DEFAULT 10,
    salary_for_passing_go integer DEFAULT 200,
    multiply_salary_for_landing_on_go integer DEFAULT 2,
    incometax integer DEFAULT 200,
    incometax_percentage integer DEFAULT 10,
    luxurytax integer DEFAULT 100,
    freeparking_payout integer DEFAULT 100,
    max_turns_in_jail integer DEFAULT 4,
    bail_amount_to_leave_jail integer DEFAULT 50,
    collect_rent_while_in_jail boolean DEFAULT true,
    wincondition varchar(20) NOT NULL DEFAULT 'LastPlayerLeft',
    end_game_when_percentage_of_players_are_bankrupt integer DEFAULT 0,
    end_game_after_amount_of_turns integer DEFAULT 0,
    end_game_when_player_has_totalworth integer DEFAULT 0,
    utility_rent_single_multiplyer integer DEFAULT 4,
    utility_rent_double_multiplyer integer DEFAULT 10,
    railroadrent integer DEFAULT 25,
    railroadmultiplayer integer DEFAULT 2,
    PRIMARY KEY (game_rule_oid)
);
ALTER TABLE game_rule OWNER TO monopoly;

CREATE TABLE characters_list (
    characters_list_oid integer REFERENCES game_p(characters_list_oid),
    character_id integer NOT NULL,
    lastactiontime timestamp DEFAULT CURRENT_TIMESTAMP, 
    active boolean DEFAULT false,
    characters_oid integer DEFAULT nextval('global_id_sequence'),
    PRIMARY KEY (characters_list_oid,character_id)
);
ALTER TABLE characters_list OWNER TO monopoly;

CREATE TABLE characters (
    characters_oid integer,
    figures_oid integer REFERENCES figures,
    player_oid integer REFERENCES player,
    position integer DEFAULT 0,
    money integer DEFAULT 0,
    state varchar(40) DEFAULT 'default',
    state_rounds integer DEFAULT 0,
    get_out_of_jail_free_count integer DEFAULT 0, 
    PRIMARY KEY (characters_oid)
);
ALTER TABLE characters OWNER TO monopoly;

CREATE TABLE field_list (
    field_list_oid integer REFERENCES game_p,
    position integer NOT NULL,
    field_oid integer DEFAULT nextval('global_id_sequence'),
    PRIMARY KEY (field_list_oid,position)
);
ALTER TABLE field_list OWNER TO monopoly;

CREATE TABLE field (
    field_oid integer REFERENCES field_list,
    field_name text NOT NULL,
    field_type text NOT NULL,
    buyable_field_oid integer DEFAULT 0,
    PRIMARY KEY (field_oid)
);
ALTER TABLE field OWNER TO monopoly;

CREATE TABLE buyable_field (
    buyable_field_oid integer REFERENCES field,
    characters_oid integer REFERENCES characters,
    price integer NOT NULL,
    mortage integer NOT NULL,
    mortaged boolean NOT NULL, 
    street_field_oid integer DEFAULT 0,
    PRIMARY KEY (buyable_field_oid)
);
ALTER TABLE buyable_field OWNER TO monopoly;

CREATE TABLE street_field (
    street_field_oid integer REFERENCES buyable_field,
    color text NOT NULL,
    base_rent integer NOT NULL,
    single_house_rent integer NOT NULL,
    double_house_rent integer NOT NULL,
    triple_house_rent boolean NOT NULL,
    quadruple_house_rent boolean NOT NULL,
    hotel_rent boolean NOT NULL,
    PRIMARY KEY (street_field_oid)
);
ALTER TABLE street_field OWNER TO monopoly;