var monopoly;


if (typeof String.prototype.startsWith != 'function') {
	  // see below for better implementation!
	  String.prototype.startsWith = function (str){
	    return this.indexOf(str) == 0;
	  };
}
if( typeof String.prototype.replaceArray != 'function' ){
	String.prototype.replaceArray = function(data) {
		var replaceString = this;
		for (key in data ) {
			if( replaceString.indexOf( "{" +key+  "}") != -1 ){
				replaceString = replaceString.replace( "{" +key+  "}" , data[key] );
			}
		}
		return replaceString;
	};
}



function init_monopoly(div_id, base_url, img_url, user)
{
	$('#board').hide();
	monopoly = new Monopoly(div_id, base_url, img_url, user);
	//monopoly = new Monopoly(div_id, 550 , 750);
}


function removeLeadingSharp(string)
{
	while( string.startsWith("#") )
		string = string.substr(1);
	return string;
}

function addLeadingSharp(string)
{
	if( ! string.startsWith("#") )
		string = "#" + string;
	return string;
}


function Monopoly(div_id, base_url, img_url, user, height, width)
{
	$.monopoly = this;
	this.div = div_id;
	this.base_url = base_url;
	this.img_url = img_url;
	this.user = user;

	this.width  = (width == undefined )? $(this.div).width() : width ;
	this.height = (width == undefined )? $(document).height(): height;
	
	$(this.div).css("height", this.height); // this is required to scroll !!!
	$(document).scrollTop( $(this.div).position().top );
	$(this.div).addClass("board");

	this.control_id = addLeadingSharp(this.div) + "_control";
	$(this.div).append("<div id=\""+removeLeadingSharp(this.control_id)+"\" class=\"control\"></div>");


	this.initControls();
	this.init();

}

Monopoly.prototype.init = function(data){
	if( data == undefined )
	{
		$.getJSON(this.base_url+"/init", function(data){
			$.monopoly.init( data ) ;
		}).fail(function(e){
			console.log("error on Monopoly::init");
			console.log(e);
		});
	}else
	{
		this.dimx = data["dimx"];
		this.dimy = data["dimy"];
		this.fields = data["fields"];
		this.players = data["players"];
		// modify fields from json data (array) to a js object with html 
		for(  pos in this.fields )
		{
			this.fields[pos] = new Field(this, this.fields[pos]);		
		}
		for( i in this.players )
		{
			this.players[i] = new Player(this, this.players[i]);
		}
		this.initPlayerLegend();

	}
	// register resize handler
	$( window ).resize(function(){
		$.monopoly.resize();
	});
	this.resize();

}

Monopoly.prototype.initControls = function(){
	/*
	 * Set user data with updateControls() called by refresh()
	 */

	var money = "<div class=\"moneybar\">Your Money: {value}</div>";
	
	$(this.control_id).append("<div class=\"title\"></div>");
	$(this.control_id).append("<a href=\"javascript:$.monopoly.refresh()\">[REFRESH]</a><br>");
	
	this.rolldiceid = addLeadingSharp(this.div + "_rolldice");
	this.finishdiceid = addLeadingSharp(this.div + "_finishdice");
	$(this.control_id).append("<a href=\"javascript:$.monopoly.roll_dice()\" id=\""+removeLeadingSharp(this.rolldiceid)+"\">[ROLL Dice]</a>");
	$(this.control_id).append("<a href=\"javascript:$.monopoly.finish_dice()\" id=\""+removeLeadingSharp(this.finishdiceid)+"\">[FINISH TURN]</a>");
	

	$(this.control_id).append(money.replaceArray({"value":0}));

	$(this.control_id).append("<a href=\""+this.base_url+"/logout\">[LOGOUT]</a>");

	this.resize();


	setTimeout("$.monopoly.refresh();",500);
}

Monopoly.prototype.initPlayerLegend = function()
{
	if( ! $(this.control_id ) )
	{
		console.log("Monopoly::initPlayerLegend(): Object #this.control_id not found");
		setTimeout("$.monopoly.initPlayerLegend();", 500);
		return;
	}
	$(this.control_id).append("<div class=\"playerlegend\"></div>") ;
	for( key in this.players )
	{
		$(this.control_id).find(".playerlegend").append("<div class=\"player player_"+this.players[key].characterid+"\">"+this.players[key].characterid + ": "+ this.players[key].name+"</div>");
	}
	this.initOwnedStreets();


}

Monopoly.prototype.initOwnedStreets = function(){
	if( ! $(this.control_id ) )
	{
		console.log("Monopoly::initOwnedStreets(): Object #this.control_id not found");
		setTimeout("$.monopoly.initOwnedStreets();", 500);
		return;
	}
	$(this.control_id).append("<div class=\"ownedstreets\"><div class=\"streettitle\">Your Streets:</div></div>");
	this.refreshOwnedStreets();
}

Monopoly.prototype.updateControls = function(new_player){
	if( this.user == new_player["name"] )
	{
		$(this.control_id).find(".title").text("Hello "+new_player["name"]+". Good Luck!");
		$(this.control_id).find(".moneybar").text("Your Money: " + new_player["money"]);
		
		if( new_player["active"] == true){
			$(this.rolldiceid).show();
			$(this.finishdiceid).show();
		}
		else{
			$(this.rolldiceid).hide();
			$(this.finishdiceid).hide();
		}
			
	}
}

Monopoly.prototype.finish_dice = function(){
	$.getJSON(this.base_url+"/finish", function(data){
		console.log( "Finish turn." );
		$.monopoly.refresh();
	}).fail(function(e){
		console.log("error on Monopoly::finish_dice");
		console.log(e);
	});
}

Monopoly.prototype.roll_dice = function(data){
	if( data == undefined )
	{
		$.getJSON(this.base_url+"/roll", function(data){
			console.log( "Rolling..." );
			$.monopoly.roll_dice( data );
		}).fail(function(e){
			console.log("error on Monopoly::roll_dice");
			console.log(e);
		});
	}else{

		if( data.length == 0 || data[0]["action"] == undefined )
		{
			console.log("Monopoly::roll_dice(): Invalid result.");
			return;
		}
		
		data[0]["dice_eye_count_one"] = parseInt(data[0]["dice_eye_count_one"]);
		data[0]["dice_eye_count_two"] = parseInt(data[0]["dice_eye_count_two"]);
		
		if( data[0]["dice_eye_count_one"]  == data[0]["dice_eye_count_two"] 
			&& data[0]["dice_eye_count_one"] != 0){
			alert("Pairs!");
		}
		
		if( data[0]["action"] == "not_your_turn" )
		{
			alert("Not your turn!");
			console.log("Monopoly::roll_dice(): Not your turn.");
			return;
			
		}else
		{	// your turn ? -> move player
			this.getPlayer( this.user ).moveTo(parseInt(data[0]["end_pos"])) ;
		}
		console.log("Player action: " + data[0]["action"]);
		
		// buy 
		if( data[0]["action"] == "buy" ){
			var c = confirm("Do you want to buy this field?");
			if( c == true )
			{
				$.getJSON(this.base_url+"/buy", function(data){
					console.log( "Buy field." );
					//console.log(data);
					$.monopoly.refreshOwnedStreets();
				}).fail(function(e){
					console.log("error on Monopoly::roll_dice: buy field");
					console.log(e);
				});
				
			}
			
		}else if( data[0]["action"] == "pay" ){
			console.log("todo: pay");

		}else if( data[0]["action"] == "DISPLAY" ){
			console.log("Monopoly::roll_dice(): action=='display'");
			alert( data[0]["action_description"] );
		} 
		this.refresh();
	}
}

Monopoly.prototype.getControlDimensions = function(){
	var ret = new Object();
	ret["width"] = 200;
	ret["height"]= this.height;
	return ret ;
}

Monopoly.prototype.getBoardDimensions = function(){
	var ret = new Object()
	ret["width"] = this.width - this.getControlDimensions()["width"];
	ret["height"]= this.height;
	return ret ;
}

Monopoly.prototype.resize = function()
{
	this.width  =  $(this.div).width()-25;
	this.height =  $(window).height();
	
	$(this.div).css("height", this.height);
	for( i in this.fields )
	{
		this.fields[i].resize();
	}
	for( i in this.players )
	{
		this.players[i].resize();
	}
	$(this.control_id).css("position", "absolute");
	$(this.control_id).css("top", $(this.div).position().top);
	$(this.control_id).css("left", $(this.div).position().left + this.getBoardDimensions()["width"]+5 ) ;
	$(this.control_id).css("width", this.getControlDimensions()["width"] );
	$(this.control_id).css("height", this.getControlDimensions()["height"] );
}

Monopoly.prototype.getPlayer = function(name)
{
	for( var i=0; i < this.players.length; i++)
	{
		if( this.players[i].name == name || 
			removeLeadingSharp(this.players[i].id)== removeLeadingSharp(name))
		{
			return this.players[i];
		}
	}
	return undefined ;
}

Monopoly.prototype.refresh = function(data){
	if( data == undefined )
	{
		$.getJSON(this.base_url+"/users", function(data){
			$.monopoly.refresh( data ) ;
		}).fail(function(e){
			console.log("error on Monopoly::refresh()");
			console.log(e);
		}).complete(function(){
			setTimeout( "$.monopoly.refresh();", 5000 );
		});
	}else
	{
		for( i in data )
		{
			var player = this.getPlayer(data[i]["name"]);
			if( player == undefined ) return ;
			player.update(data[i]);
			if( this.user == data[i]["name"] )
			{
				this.updateControls( data[i] );
			}
		}
	}
}


Monopoly.prototype.refreshOwnedStreets = function(data){
	if( data == undefined )
	{
		$.getJSON(this.base_url+"/fields", function(data){
			$.monopoly.refreshOwnedStreets( data ) ;
		}).fail(function(e){
			console.log("error on Monopoly::refreshOwnedStreets()");
			console.log(e);
		});
	}else
	{
		console.log("Refresh owned streets");
		var field = $(this.control_id).find(".ownedstreets")[0];
		if( $(field) == undefined ){
			console.log("Failed refreshing OwnedStrests: div does not exists.");
			return ;
		}
		// start with a clean div (don't delete the description/title)
		$(field).children("div").remove(".gamefields");
		$(field).append("<div class=\"gamefields\"></div>");
		var field = $(field).find(".gamefields")[0];
		for( i in data )
		{
			var css = "background-color: " + data[i]["color"] + ";";
			if( data[i]["owner"] == this.user ){				
				$(field).append("<div class=\"gamefield\"><div class=\"colorbar\" style=\""+css+"\"></div>" +data[i]["name"]+  "</div>");
			}
		}
		$(field).append("<div style=\"float:clear;\"></div>");
	
	}
}

function Player(parent, player)
{
	this.monopoly = parent ;
	player["id"] = player["name"];
	
	this.id = addLeadingSharp(this.monopoly.div)+"_player_" + player["id"];
	this.name = player["name"];
	this.pos = parseInt(player["pos"]);
	this.money = parseInt(player["money"]);
	this.state = player["state"];
	this.characterid = player["character_id"];
	this.active = player["active"];

	var player_class = "player_" + this.characterid;
	
	$(this.monopoly.div).append("<div class=\"player "+player_class+"\" id=\""+removeLeadingSharp(this.id)+"\" ></div>");
	
	
	$(this.id).player = this ;
	$(this.id).text(this.characterid);
	
	
	$(this.id).click(function(){
		$.monopoly.getPlayer(this.id).moveRel(+1);
	});
}

Player.prototype.getField = function(pos)
{
	if( pos == undefined)
		pos = this.pos ;
	if( pos >= this.monopoly.fields.length )
	{
		console.log("getField out of range: " + pos);
		return -1;
	}
	return this.monopoly.fields[pos];
}

Player.prototype.resize = function()
{
	var field_id = this.getField(this.pos).id; 
	
	if( $(field_id) != undefined )
	{
		$(this.id).detach().appendTo(field_id);
	}else
	{
		console.log("Div for player '" + this.name + "' at pos '"+this.pos+"' is not ready.");
		$(this.id).hide();
	}
}

Player.prototype.moveRel = function(rel_pos)
{
	return this.moveTo(this.pos + rel_pos);
}

Player.prototype.moveTo = function(pos)
{
	pos = parseInt(pos);
	var duration = 2000 ;
	
	//var field_id = this.getField(pos).id; 
	//$(this.id).fadeOut().delay(500).hide().detach().appendTo(field_id).fadeIn();
	
	var curr_field_id = this.getField(this.pos).id;
	var dest_field_id = this.getField(pos).id;
	var src_pos = $(dest_field_id).position();
	var top_offset  = $(dest_field_id).position().top  - $(curr_field_id).position().top;
	var left_offset = $(dest_field_id).position().left - $(curr_field_id).position().left;
	
	$(this.id).css("position","relative");
	$(this.id).css("top" , 0);
	$(this.id).css("left" , 0);
		
	$(this.id).animate({
		top:"+="+top_offset+"px", 
		left: "+="+left_offset+"px"
		}, duration
	);
	setTimeout("(function(){$('"+this.id+"').detach().appendTo('"+dest_field_id+"').css('position','static');}())" 
		, duration
	);
	this.pos = pos ;
}

Player.prototype.update = function(data){
	if( data == undefined )
	{
		console.log("Warning: '"+this.name+"'.update without data");
		return ; 
	}
	
	if( data["name"] != this.name )
	{
		console.log("Warning '"+this.name+"'.update with wrong username '"+data["name"]+"'");
		return ;
	}
	
	if( this.pos != parseInt(data["pos"]) ){
		this.moveTo( data["pos"] );
	}
}

function Field(parent, field)
{
	this.monopoly = parent;
	this.id = addLeadingSharp(this.monopoly.div)+"_field_"+field["pos"];
	this.name = field["name"];
	this.pos = parseInt(field["pos"]);
	this.type = field["type"];
	this.houses = field["houses"];
	this.event_type = "";
	var img = "<img src=\"{src}\" alt=\"{alt}\" />";


	$(this.monopoly.div).append("<div id=\""+removeLeadingSharp(this.id)+"\"></div>");
	
	$(this.id).addClass("gamefield");
	$(this.id).html( "<div class=\"title\">" + this.name + "</div>");
	if( this.type == "street" )
	{
		$(this.id).addClass("gamestreet");
		$(this.id).prepend("<div class=\"colorbar\"></div>")
		$(this.id).find(".colorbar").append("<div class=\"houses\"></div>");
		$(this.id).append("<div class=\"players\"></div>");
		
		if( this.houses == 5 ){
			$(this.id).find(".houses").append( img.replaceArray(  {"src": this.monopoly.img_url + "/hotel.png", "alt": "hotel"}  ));
		}else if (this.houses > 0 && this.houses < 5){
			for( var i=0; i< this.houses; i++)
			{
				$(this.id).find(".houses").append(img.replaceArray(  {"src": this.monopoly.img_url + "/house.png", "alt": "house"} ) );
			}
		}

		this.color = field["color"];
		$(this.id).find(".colorbar").css("background-color", this.color).height(20);

	}else if( this.type == "event")
	{
		if( field["event_type"] != undefined )
			this.event_type = field["event_type"];
		
		if( this.event_type == "Railroad"){
			$(this.id).css("background-image", "url(\"" + this.monopoly.img_url + "/train.png\")" );
			$(this.id).css("background-position", "center center");
			$(this.id).css("background-size", "40px 40px");
			$(this.id).css("background-repeat", "no-repeat");


		}
	}
}

Field.prototype.getPositionDescription = function()
{
	var field_count = 2*this.monopoly.dimx + 2*this.monopoly.dimy -4;
	if( this.pos == 0 || 
		this.pos == this.monopoly.dimx-1 ||
		this.pos == this.monopoly.dimx + this.monopoly.dimy -2 ||
		this.pos == this.monopoly.dimx + 2*this.monopoly.dimy -3 )
	{
		return "corner";
	}else if( this.pos < this.monopoly.dimx )
	{ // bottom <<-----
		return "bottom";
	}else if( this.pos < this.monopoly.dimx + this.monopoly.dimy-2)
	{ // left from bottom to top
		return "left";
	}else if( this.pos >= -2+this.monopoly.dimx + this.monopoly.dimy && this.pos < -2+this.monopoly.dimx + 2*this.monopoly.dimy )
	{ // top from left to right
		return "top";
	}else if( this.pos > field_count - this.monopoly.dimy && this.pos < field_count)
	{ // right from top to bottom
		return "right"
	}else
	{
		return undefined;
	}
}

Field.prototype.adjustPos = function()
{
	return this.resize();
}


Field.prototype.resize = function()
{	
	var field_width =  this.monopoly.getBoardDimensions()["width"] / this.monopoly.dimx;
	var field_height = this.monopoly.getBoardDimensions()["height"] / this.monopoly.dimy;
	var field_count = 2*this.monopoly.dimx + 2*this.monopoly.dimy -4;
	
	var offset_x = $(this.monopoly.div).position().top;
	var offset_y = $(this.monopoly.div).position().left;
	
	$(this.id).width( field_width);
	$(this.id).height( field_height);

	$(this.id).css("position","absolute");
	var top = 0 ;
	var left = 0 ;
	if( this.pos < this.monopoly.dimx )
	{ // bottom <<-----
		top = (this.monopoly.dimy-1)* field_height;
		//left = this.monopoly.width - field_width - field_width*this.pos;
		left = (this.monopoly.dimx - this.pos -1 ) * field_width ;
	}else if( this.pos < this.monopoly.dimx + this.monopoly.dimy-2)
	{ // left from bottom to top
		//top = this.monopoly.height - 2*field_height - (this.pos-this.monopoly.dimx)*field_height ;
		top = ( this.monopoly.dimx + this.monopoly.dimy-2 - this.pos ) * field_height;
		left = 0 ;
		
	}else if( this.pos >= -2+this.monopoly.dimx + this.monopoly.dimy && this.pos < -2+this.monopoly.dimx + 2*this.monopoly.dimy )
	{ // top from left to right
		top = 0; 
		left = (this.pos - (this.monopoly.dimx + this.monopoly.dimy-2) )*field_width;
		//left = 2*field_width + field_width*(this.pos-(this.monopoly.dimx + this.monopoly.dimy));
	
	}else if( this.pos > field_count - this.monopoly.dimy && this.pos < field_count)
	{ // right from top to bottom
		//top = field_height + field_height*(-2+this.pos-(field_count - this.monopoly.dimy)) ;
		top = (this.pos - (2*this.monopoly.dimx + this.monopoly.dimy-3)) * field_height;
		left = (this.monopoly.dimy -1)* field_width ;
	}else
	{
		$(this.id).hide();
		console.log("Ignored field " + this.pos);
	}

	$(this.id).css("top",top+offset_x);
	$(this.id).css("left",left+offset_y);
}









