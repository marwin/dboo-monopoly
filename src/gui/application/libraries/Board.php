<?php

class Board{
	
	var $dimX;
	var $dimY;
	var $CI; 
	var $fields;
	var $player;
	
	public function __construct($dimX=0, $dimY=0)
	{
		// load framework reference 
		$this->CI =& get_instance();
		$this->CI->load->library("Field");
		$this->CI->load->library("Player");
		
		$this->dimX = $dimX;
		$this->dimY = $dimY;
		$this->fields = array();
		$this->player = array();
	}
	
	protected function isFramePos($x, $y, $dimx, $dimy)
	{
		if( $x == 0 || $x == $dimx-1)
		{
			return true;
		}
		if( $y == 0 || $y == $dimy-1)
		{
			return true;
		}
		return false ;
	}

	/**
	 * Translates grid coordinates to ring coordinates (index of array)
	 */
	protected function switchCoordinates($current, $x, $y, $dimx, $dimy, $start="bottomright")
	{
		if( $start == "topleft")
		{
			// --->
			if( $y == 0 )
			{
				return $current;
			// |
			// |
			// v
			}elseif ( $x == $dimx-1 )
			{
				return ($dimx-1) + $y ;
				// <---
			}elseif ($y == $dimy-1)
			{
				return ($dimx-1) + ($dimy-1) + ($dimx-1)-$x ;
			}
			// ^
			// |
			// |
			else{
				return 2*($dimx-1) + ($dimy-1) + ($dimy-1)-$y;
			}
		}else {
			// --->
			if( $y == 0 && $x != 0 )
			{
				return ($dimx-1) + ($dimy-1)-$y + $x;
			// |
			// |
			// v
			}elseif ( $x == $dimx-1 && $y != $dimy-1)
			{
				return ($dimx-1) + ($dimy-1) + $x + $y ;
			// <---
			}elseif ($y == $dimy-1)
			{
				return ($dimx-1) - $x ;
			}
			// ^
			// |
			// |
			else{
				return ($dimx-1) + ($dimy-1)-$y ;
			}
				
		}
	}

	/**
	 * Returns how much gamefields we have.
	 * @return number
	 */
	public function getFieldCount()
	{
		return $this->dimX * $this->dimY - ($this->dimX-2)*($this->dimY-2);
	}
	
	/**
	 * Returns if a board is complete
	 * @return bool
	 */
	public function isComplete()
	{
		if( count($this->fields ) == $this->getFieldCount() )
		{
			// check all fields 
			for($i=0; $i < $this->getFieldCount(); $i++ )
			{
				if( !isset($this->fields[$i])  ||  ! $this->fields[$i] instanceof Field )
				{
					log_message("error", "Board::isComplete() found invalid field on ".$i );
					return False ;
				}
			}
		}else {
			return False ;
		}
		return True;
	}
	
	/**
	 * Append a field to our game board. The Position from the field will be used.
	 * @param Field $field
	 */
	public function appendField( $field )
	{
		if( ! $field instanceof Field )
		{
			log_message("error", "Board::switchCoordinates() got an invalid field!");
			return;
		}
		$this->fields[$field->pos] = $field ;
	}
	
	/**
	 * Append a player
	 * @param unknown $player
	 */
	public function appendPlayer( $player )
	{
		if( $player instanceof Player )
		{
			$this->player[] = $player;
			return ;
		}
		log_message("error", "Boad::appendPlayer() got an invalid player!");
	}
	
	/**
	 * Returns all Players on a specific field 
	 * @param int $pos
	 * @return array <Player:, Player>
	 */
	protected function getPlayersForField( $pos )
	{
		$ret = array();
		foreach($this->player AS $player)
			if( $player->pos == $pos )
				$ret[] = $player;
		return $ret ;
	}
	
	/**
	 * Returns the string representation for this board
	 * containing the game board and the contol interface.
	 * @param string $prefix
	 * @param string $end
	 * @return string
	 */
	public function render($prefix="\t",$end="")
	{
		$board = $this->renderBoard($prefix."\t\t\t","");
		$control = $this->renderControlls($prefix."\t\t\t","");
		$html = <<<END
$prefix<table id="board">
$prefix\t<tr>
$prefix\t\t<td style="vertical-align:top;">
$board
$prefix\t\t</td>
$prefix\t\t<td style="vertical-align:top;">
$control
$prefix\t\t</td>
$prefix\t</tr>
$prefix</table>$end
END;
		
		return $html ;
	}
	
	public function renderControlls($prefix="\t",$end="")
	{
		
		$html = $this->CI->load->view('board/controls', array(), TRUE);
		/*
		$button = "<a href=\"{link}\">{text}</a>";
		
		$data = array(
				"{link}" => site_url($this->CI->config->item("URL_NEXT_STEP")), 
				"{text}" => "Next Step"
		) ;
		
		
		$html = "$prefix<div class=\"control\">";
		$html .= strtr(
					$button,
				 	$data
				);
		$html .= "</div>";
		*/
		return  $html.$end;
	}
	
	public function renderBoard($prefix="\t",$end="")
	{
		$current = 0 ;
		$html = $prefix."<table class=\"board\">\n";
		for($y=0; $y < $this->dimY; $y++ )
		{
			$html .= $prefix."\t<tr>\n";
			for($x=0; $x < $this->dimX; $x++ )
			{
				$html .= $prefix."\t\t<td>\n";
				
				if( $this->isFramePos($x, $y, $this->dimX, $this->dimY))
				{
					$ring_index = $this->switchCoordinates($current, $x, $y, $this->dimX, $this->dimX);
					if( isset($this->fields[$ring_index]) && $this->fields[$ring_index] instanceof Field )
					{
						$p = $this->getPlayersForField($ring_index);
						$html .= $this->fields[$ring_index]->render($prefix."\t\t\t",$end="\n", $p);
						$current++;
					}else{
						log_message("error", "Board::render() got an invalid field instance!");
						$html .= "$prefix.\t\t\t". $x." ".$y."\n";
					}
				}
				$html .= $prefix."\t\t</td>\n";
			}
			$html .= $prefix."\t</tr>\n";
		}
		$html .= $prefix."</table>".$end;
		return $html ;
		
	}
	
	public function to_array()
	{
		$ret = array();
		$ret["dimx"] = $this->dimX;
		$ret["dimy"] = $this->dimY;
		$ret["fields"] = array();
		foreach($this->fields as $field)
			$ret["fields"][] = $field->to_array();
		$ret["players"] = array();
		foreach($this->player as $player1)
			$ret["players"][] = $player1->to_array();
		
		return $ret ;
	}
}

