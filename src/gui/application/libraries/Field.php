<?php

class Field{
	var $pos ;
	var $name;
	var $CI;

	public function __construct($name="", $pos=-1){
		$this->name = $name;
		$this->pos = $pos ;
		$this->CI =& get_instance();
	}
	
	protected function renderPlayers($players, $prefix="\t", $end="")
	{
		$html = "";
		foreach( $players AS $player)
		{
			if( $player instanceof Player )
				$html .= $player->render($prefix,"") ."<br/>\n" ;
		}
		return $html.$end;
	}
	
	public function to_array()
	{
		return array( "name"=>$this->name, "pos"=>$this->pos, "type"=>"field" );
	}
	
	public function render($prefix="\t",$end="",$players=array())
	{
		$html = <<<END
$prefix<div class="{classnames}">
$prefix\t<div class="{fieldsets_main}">{title}
{main}
$prefix\t</div>
$prefix</div>$end
END;
		$data = array( 
			"{classnames}"	=> "gamefield ", 
			"{fieldsets_main}" => "gamestreet_main",
			"{title}" 		=> $this->name,
			"{main}" 		=> $this->renderPlayers($players, $prefix."\t\t","")
		);
		return strtr( $html, $data );
	}
	
}
//  Besitzt ein Spieler alle Straßen eines Monopols, ist die Miete für diese in unbebautem Zustand doppelt so hoch.
class Street extends Field{
	var $color = "blue";
	var $owner ;
	var $price ;
	var $houses = 0; // vllt.
	// Preise für Häuser
	var $price_house;
	var $price_hotel;
	// Übernachtungspreise 
	var $rent = array(
		0=> 1, // no house
		1=> 1,
		2=> 1,
		3=> 1,
		4=> 1, // 4 houses
		5=> 1 // hotel
	);// Übernachtungspreise für Grundstück, Häuser, Hotel 

	public function __construct($name="", $pos=-1, $houses=0){
		parent::__construct($name,$pos);
		$this->houses = $houses;
	}
	
	public function set_data($data)
	{
		$this->name = $data["field_name"];
		$this->pos = $data["pos"];
		$this->color = $data["color"];
		$this->type = $data["field_type"];
		$this->price = $data["price"];
		$this->mortage = $data["mortage"];
		$this->owner = $data["owner"];
		$this->houses = $data["house_count"];
		$this->base_rent = $data["base_rent"];
		$this->single_house_rent = $data["single_house_rent"];
		$this->double_house_rent = $data["double_house_rent"];
		$this->triple_house_rent = $data["triple_house_rent"];
		$this->quadruple_house_rent = $data["quadruple_house_rent"];
		$this->hotel_rent = $data["hotel_rent"];
	}
	
	public function to_array()
	{
		return array( 
				"name"=>$this->name, 
				"pos"=>$this->pos, 
				"type"=>"street", 
				"color"=>$this->color,
				"houses"=>$this->houses,
				"owner" => $this->owner
		);
	}
	
	
	public function getColorClasses( $base_color )
	{
		$colors = array(
			"white"	=> array( "b_white", 	"t_black"),
			"black"	=> array( "b_black", 	"t_white"),
			"red" 	=> array( "b_red", 		"t_white"),
			"orange"=> array( "b_orange", 	"t_black"),
			"blue"	=> array( "b_blue", 	"t_white"),
		);
		if( ! array_key_exists( $base_color, $colors ))
		{
			log_message("error", "Street::getColors color '".$base_color."' not found!"  );
			return $colors["white"];
		}
		return $colors[$base_color];
	}
	
	
	public function render($prefix="\t",$end="",$players=array())
	{
		$html = <<<END
$prefix<div class="{classnames}">
$prefix\t<div class="{fieldsets}">{title}</div>
$prefix\t<div class="{fieldsets_main}">
{main}
$prefix\t</div>
$prefix\t<div class="houses">{house}</div>
$prefix</div>$end
END;
		$colors = $this->getColorClasses($this->color);
		$data = array( 
			"{classnames}"	=> "gamefield gamestreet ", 
			"{fieldsets}"	=> "gamestreet_top " . implode(" ", $colors),
			"{fieldsets_main}" => "gamestreet_main",
			"{title}" 		=> $this->name,
			"{main}" 		=> $this->renderPlayers($players, $prefix."\t\t", ""),
			"{house}"		=> $this->renderHouses($prefix."\t\t",$end="")
		);
		return strtr( $html, $data );
	}
	
	public function renderHouses($prefix="\t",$end="")
	{
		$html = "";
		if( $this->houses > 0 )
		{
			if( $this->houses == 5 )
			{	// make a hotel
				$html .= "<img alt=\"hotel\" src=\"".base_url()."/images/".$this->CI->config->item("img_hotel"). "\"/>";
			}else{
				for($i=0; $i< $this->houses; $i++ )
				{
					$html .= "<img alt=\"house\" src=\"".base_url()."/images/".$this->CI->config->item("img_house"). "\"/>";
				}
			}
		}
		return $html;
	}
}

class EventField extends Field{
	public function __construct($name="", $pos=-1, $event_type="event"){
		parent::__construct($name,$pos);
		$this->event_type = $event_type;
	}

	public function to_array()
	{
		return array(
				"name"=>$this->name,
				"pos"=>$this->pos,
				"type"=>"event",
				"event_type" => $this->event_type
		);
	}
	
	public function render($prefix="\t",$end="",$players=array())
	{
		$html = <<<END
$prefix<div class="{classnames}">
$prefix\t<div class="{fieldsets_main}">{title}
{main}
$prefix\t</div>
$prefix</div>$end
END;
		$data = array( 
			"{classnames}"	=> "gamefield gameevent", 
			"{fieldsets_main}" => "gamestreet_main",
			"{title}" 		=> $this->name,
			"{main}" 		=> $this->renderPlayers($players, $prefix."\t\t", "")
		);
		return strtr( $html, $data );
	}
	
}
