<?php
/*
 object(stdClass)#26 (8) {
	 ["username"]=>
	 	string(10) "testuser03"
	 ["character_id"]=>
	 	string(1) "2"
	 ["lastactiontime"]=>
	 	string(26) "2015-01-22 19:29:13.077647"
	 ["pos"]=>
	 	string(1) "0"
	 ["money"]=>
	 	string(4) "1500"
	 ["state"]=>
	 	string(7) "default"
	 ["state_rounds"]=>
	 	string(1) "0"
	 ["get_out_of_jail_free_count"]=>
	 	string(1) "0"
 }
 */
class Player
{
	var $name ;
	var $pos;
	var $character_id;
	var $lastactiontime;
	var $money;
	var $state ;
	var $state_rounds ;
	var $get_out_of_jail_free_count;
	
	public function __construct($name="", $pos=-1)
	{
		$this->name = $name;
		$this->pos = $pos;
	}
	
	
	public function render($prefix="\t",$end="")
	{
		return <<<END
$prefix<div class="player">$this->name</div>$end
END;
	}
	
	public function to_array()
	{
		return array(
			"name"=> $this->name, 
			"pos" => $this->pos,
			"character_id" => $this->character_id,
			"lastactiontime"  => $this->lastactiontime,
			"money" => $this->money,
			"state" => $this->state,
			"state_rounds" => $this->state_rounds,
			"get_out_of_jail_free_count" => $this->get_out_of_jail_free_count,
			"active" => $this->active
		);
	}
	
	public function set_data($data)
	{
		$this->character_id = $data["character_id"];
		$this->lastactiontime = $data["lastactiontime"];
		$this->money = $data["money"];
		$this->state = $data["state"];
		$this->state_rounds = $data["state_rounds"];
		$this->get_out_of_jail_free_count = $data["get_out_of_jail_free_count"];
		$this->active = $data["active"];
	}
}