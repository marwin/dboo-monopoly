<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Welcome extends CI_Controller {
	var $data = array();
	
	public function __construct()
	{
		parent::__construct();
		$this->load->library('parser');
		$this->load->model("Gamefield");
		
		$this->data = array(
			'header' => $this->load->view('header', '', TRUE),
			'footer' => $this->load->view('footer', '', TRUE),
		);
	} 
	
	public function index()
	{
		$this->data["heading"] = "Welcome to Monopoly!"; 

		$game = site_url("game");

		$this->data["content"] = <<<END
This is a Monopoly game! <br/>
<a href="$game">Play it!</a>
END;
		$this->parser->parse('welcome_message', $this->data);
	}
	
	public function game($id)
	{
		redirect("game/start", "refresh");
		$id = 1 ; // TODO: remove this 
		$this->data["heading"] = "Playing game '$id'"; 
		$game = array("gamefield" => $this->Gamefield->get( $id ) );
		$this->data["content"] = $this->load->view('gamefield', $game, TRUE);
		
		$this->parser->parse('welcome_message', $this->data);
		
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
