<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Game extends CI_Controller {
	var $data = array();
	
	public function __construct()
	{
		parent::__construct();
		$this->load->driver('cache');
		$this->load->library('session');
		$this->load->library('parser');
		$this->load->model("Gamefield");
		$this->load->model("dice");
		
		$this->data = array(
			'header' => $this->load->view('header', '', TRUE),
			'footer' => $this->load->view('footer', '', TRUE),
		);
	}
	
	public function index()
	{
		$this->data["heading"] = "Select your game!"; 
		
		// TODO: load all available games
		$data = array(
				"games" => array("640d26ad-b496-4430-afb6-4efd417c0f11" => "Game 1")
		);
		$this->data["content"] = $this->load->view('game/index', $data, TRUE);
		
		$this->parser->parse('welcome_message', $this->data);
	}
	
	/**
	 * "Login" function which sets session variables
	 * or redirect if session already exists
	 */
	public function start()
	{
		if( !empty($this->input->post("username")) )
		{
			$username = $this->input->post("username");
			$game = $this->input->post("game");

			$this->session->set_userdata("username", $username);
			$this->session->set_userdata("game", $game);
			redirect("game/play/", "refresh"); // end form-post with refreshing the page
			// returns/ends hear
		}
			
		if( !empty( $this->session->userdata('username') ))
		{
			redirect("game/play/", "location");
			// returns/ends hear
		}

		redirect("game/index");
		
	}
	/*
	 * Logout function will destroy our session and redirect to index
	 */
	public function logout()
	{
		$this->session->sess_destroy();
		redirect("game/index", "refresh");
	}
	
	protected function getLoginOrExitWithJSON()
	{
		$username = $this->session->userdata('username' );
		$game = $this->session->userdata('game' );
		if( empty($username) )
		{
			$this->output
			->set_content_type('application/json')
			->set_output(json_encode(array("status"=>403, "description"=>"error")));
		}else {
			return array( "game" => $game, "username" => $username );
		}
	}
	/*
	 * Init function returns a json-description of our gamefield
	 * A valid Session is required! 
	 */
	public function init()
	{
		$login = $this->getLoginOrExitWithJSON();
		
		$game = $this->Gamefield->get( $login["username"], $login["game"] ) ;
		$data = $game->to_array();
		
		$this->output
			->set_content_type('application/json; charset=utf-8')
			->set_output(json_encode($data));
	}	
	
	public function roll()
	{
		$login = $this->getLoginOrExitWithJSON();
		$data = $this->dice->roll( $login["username"], $login["game"] ) ;
		$this->output
			->set_content_type('application/json; charset=utf-8')
			->set_output(json_encode($data));
	}
	
	public function finish()
	{
		$login = $this->getLoginOrExitWithJSON();
		$data = $this->dice->finish( $login["username"], $login["game"] ) ;
		$this->output
			->set_content_type('application/json; charset=utf-8')
			->set_output(json_encode($data));
	}
	
	public function buy(){
		$login = $this->getLoginOrExitWithJSON();
		$this->load->model("CharacterList");
		$ret = $this->CharacterList->buy( $login["username"], $login["game"] ) ;
		$this->output
		->set_content_type('application/json; charset=utf-8')
		->set_output(json_encode($ret));
	}
	
	/*
	 * Returns users as JSON-Data
	 */
	public function users()
	{
		$login = $this->getLoginOrExitWithJSON();
		
		$this->load->model("CharacterList");
		$users = $this->CharacterList->get( $login["username"], $login["game"] ) ;
		
		$data = array();
		foreach($users as $key => $player)
		{
			$data[$key] = $player->to_array();
		}
		$this->output
		->set_content_type('application/json; charset=utf-8')
		->set_output(json_encode($data));
	}
	
	/*
	 * Returns streets/fields as JSON-Data
	 */
	public function fields()
	{
		$login = $this->getLoginOrExitWithJSON();
	
		$this->load->model("FieldList");
		$fields = $this->FieldList->get( $login["game"] ) ;
	
		$data = array();
		foreach($fields as $key => $field)
		{
			$data[$key] = $field->to_array();
		}
		$this->output
		->set_content_type('application/json; charset=utf-8')
		->set_output(json_encode($data));
	}
	
	/*
	 * Standard play method which generates the game board
	 */
	public function play()
	{
		$username = $this->session->userdata('username' );
		$game = $this->session->userdata('game' );
				
		if( empty($username) )
		{
			redirect("game/start", "location");
		}
		
		$this->data["heading"] = "Playing game '$game'"; 
		try{
			$game_data = array(
					"gamefield" => $this->Gamefield->get( $username, $game ),
					"user"=> $username
			);
			$this->data["content"] = $this->load->view('gamefield', $game_data, TRUE);
		}catch( Exception $e)
		{
			log_message("error", "Exception on game/play");
		}
		
		
		$this->parser->parse('welcome_message', $this->data);
	}
	
	public function action( $type="" )
	{
		if( $type == "next" )
		{
			redirect("game/play", "location");
		}
		
		// kill all form-posts with a clean refresh
		redirect("game/index", "refresh");
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
