<?php if(isset($gamefield)): ?>

<style type="text/css">
.control
{
	vertical-align: top;
}
.control a
{
	border: 2px solid gray; 
	padding: 2px;
	text-decoration: none;
}
.control a:hover
{
	border: 2px solid orange; 
	padding: 2px;
}


.board
{
}

.board .gamefield 
{
	border: 1px solid gray;
	width: 100px;
	height: 100px ;
	text-align: center ;
	word-wrap: break-word;
}
.board .gamestreet 
{
}
.board .gamestreet .gamestreet_main
{
	height: 45px;
}
.board .gamestreet .houses img
{
	width:20px;
	height:20px;
}
.board .gamestreet .gamestreet_top
{
	height: 25px ;
	padding-top: 7px;
}
.board .gameevent
{
	color: red ;
}

.board .player
{
	text-align: left; 
	margin:2px; 
	color: black ;
	display: inline;
	border: 3px solid ;
	background-color : white;
}
.board .player_0{ border-color: red; }
.board .player_1{ border-color: blue; }
.board .player_2{ border-color: green; }
.board .player_3{ border-color: orange; }

.playerlegend { 
	display: block;
	margin-top: 30px; 
}

.playerlegend .player {
	display: block;
	margin: 1px ;
}

.ownedstreets .gamefield .colorbar{
	height: 20px;
}
.ownedstreets .gamefield{
	width :70px;
	height : 70px ;
	#display: inline-block;
	float: left ;
}

/* http://www.w3schools.com/htmL/html_colornames.asp */
.t_red { 	color: red ; }
.t_black{	color: black;}
.t_green{	color: green;}
.t_white{	color: white;}

.b_red { 	background-color: red ; }
.b_blue { 	background-color: blue ; }
.b_black{	background-color: black;}
.b_green{	background-color: green;}
.b_white{	background-color: white;}
.b_orange{	background-color: orange;}
.b_yellow{ 	background-color: yellow;}
.b_aqua{	background-color: aqua; }
.b_chocolate{background-color:chocolate;}
.b_darkmagenta{background-color:darkmagenta;}
</style>
<script>
$(document).ready(function(){
	init_monopoly("#monopoly", "<?=site_url("game");?>", "<?=base_url("images");?>", "<?=$user?>");
});
</script>
<div id="monopoly"></div>

<?php endif ; ?>
