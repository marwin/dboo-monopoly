<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class FieldList extends Monopoly {

	var $qstring = "SELECT * FROM get_field_list('{id}');";
	
    public function __construct()
    {
        // Call the Model constructor
        parent::__construct();
        $this->load->library("Field");
    }
    
    
    public function get($id)
    {
    	$user = "testuser01";
    	$ret = array();
		
    	try{
    		$data = array("id"=> $id);
    		$qres = $this->query($this->qstring, $data, $user);
    		 
    		$fields = array();
    		foreach ($qres as $row)
    		{
    			$field = array(
    					"name" => $row->field_name,
    					"field_name" => $row->field_name,
    					"pos" => $row->pos,
    					"type" => $row->field_type,
    					"field_type" => $row->field_type,
    					"price" => $row->price,
    					"mortage" => $row->mortage,
    					"owner" => $row->owner,
    					"color" => $row->color,
    					"house_count" => $row->house_count,
    					"houses" => $row->house_count,
    					"base_rent" => $row->base_rent,
    					"single_house_rent" => $row->single_house_rent,
    					"double_house_rent" => $row->double_house_rent,
    					"triple_house_rent" => $row->triple_house_rent,
    					"quadruple_house_rent" => $row->quadruple_house_rent,
    					"hotel_rent" => $row->hotel_rent
    			);
    			$fields[] = $field;
    		}
    		
    		foreach($fields as $field)
    		{
    			if( $field["type"] == "street" || $field["type"] == "Street")
    			{
    				$s = new Street($field["name"], $field["pos"]);
    				$s->set_data($field);
    				$ret[] =  $s ;
    			}elseif( $field["type"] == "event" )
    			{
    				$ret[] =  new EventField($field["name"], $field["pos"]) ;
    			}elseif( $field["type"] == "station" || $field["type"] == "Railroad")
    			{
    				$ret[] = new EventField($field["name"], $field["pos"], "Railroad");
    			}else {
    				$ret[] = new Field($field["name"], $field["pos"]) ;
    			}
    		}
    	}catch(Exception $e)
		{
			log_message("error" , "Catch on FieldList::get()" . $e->getMessage());
		}
		return $ret;
	}
}
