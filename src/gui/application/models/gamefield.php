<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Gamefield extends CI_Model {

    var $dimX   = 11;
    var $dimY	= 11;

    public function __construct()
    {
        // Call the Model constructor
        parent::__construct();
        $this->load->library("Board");
        $this->load->model("CharacterList");
        $this->load->model("FieldList");
    }
    
    public function get($user, $id)
    {    	
    	/*
		$user = "testuser01";
    	$pw   = "gpHMMrI2Lq1a";
    	$dsn = "postgre://".$user.":".$pw."@ishi.informatik.tu-chemnitz.de/monopoly?char_set=utf8&dbcollat=utf8_general_ci";   
    	$this->load->database($dsn);
		*/

    	$result = new Board( $this->dimX, $this->dimY );
		
		try{
			$players = $this->CharacterList->get($user, $id);
			foreach($players as $player)
				$result->appendPlayer($player);
			
			
			$fields = $this->FieldList->get($id);
			foreach($fields as $field)
				$result->appendField($field);
		}catch(Exception $e)
		{
			log_message("error" , "Catch on Gamefield::get()" . $e->getMessage());
		}
	
		
		return $result;
	}
}
