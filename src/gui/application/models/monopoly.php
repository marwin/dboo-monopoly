<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
 * Monopoly Model will be loaded automatically
 */
class Monopoly extends CI_Model {

	var $testusers = array("monopoly", "testuser01","testuser02","testuser03","testuser04");
	var $testpassword = "gpHMMrI2Lq1a";
        var $monopolypwd = "tKmqNpKc70Qr";

    public function __construct()
    {
        // Call the Model constructor
        parent::__construct();
        
    }
    
    protected function prepareQuery($string, $data)
    {
    	foreach($data as $key => $val )
    	{
    		$string = str_replace( "{$key}", pg_escape_string($val), $string);
    	}    	
    	log_message("info", "Build sql query: ".$string);
    	return $string; 
    }

    protected function query($query, $data, $user, $password=NULL)
    {
    	if( empty($password) )
    	{
    		if( in_array($user, $this->testusers) )
    		{
    			$password = $this->testpassword;
                        if( $user == "monopoly")
                                $password = $this->monopolypwd;
    		}
    	}
    	log_message("info", "SQL-Query for '$user'");
    	$dsn = "postgre://".$user.":".$password."@ishi.informatik.tu-chemnitz.de/monopoly?char_set=utf8&dbcollat=utf8_general_ci";   
    	$this->load->database($dsn);

    	$q = $this->db->query($this->prepareQuery($query,$data));
    	
    	$this->db->close();
    	return $q->result();
    }
}
