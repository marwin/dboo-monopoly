<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dice extends Monopoly {

	var $q_roll = "SELECT * FROM roll_dice('{id}');";
	var $q_finish = "SELECT * FROM finish_turn('{id}');";
	public function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

    public function roll($user, $game_id)
    {
    	$data = array("id"=> $game_id);
    	
    	log_message("info", "Rolling for '$user' on '$game_id'");
    	$qres = $this->query($this->q_roll, $data, $user);
	
    	$events = array();
    	foreach ($qres as $row)
    	{
			$event = array();
			foreach($row as $key => $val)
			{
				$event[$key] = $val ;
			}
			$events[] = $event;
    	}
    	/*
    	 * if action is "pay" :
    	 * 		pay the rent by clicking a button
    	 * 
    	 * action: "buy"
    	 * action_description: ""
    	 * action_target: ""
    	 * action_value: "120"
    	 * dice_eye_count_one: "3"
    	 * dice_eye_count_two: "6"
    	 * end_pos: "9"
    	 * start_pos: "0"
    	 */
    	return $events;
    }
    public function finish($user, $game_id)
    {
    	$data = array("id"=> $game_id);
    	
    	log_message("info", "Finish turn for '$user' on '$game_id'");
    	$qres = $this->query($this->q_finish, $data, $user);
    	
		return ;
	}
}
