<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class CharacterList extends Monopoly {

	var $qstring = "SELECT * FROM get_character_list('{id}');";
	var $buyQString = "SELECT * FROM buy_field('{id}');";
	
	public function __construct()
    {
        // Call the Model constructor
        parent::__construct();
        $this->load->library("Player");
    }

    public function get($user, $id)
    {
		$is_first = True ;
    	$ret = array();
    	
		try{
			$data = array("id"=> $id);
			$qres = $this->query($this->qstring, $data, $user);
			
			$characters = array();
			foreach ($qres as $row)
			{
				$character = array();
				$character["active"] = $is_first ;
				$is_first = False ;
				foreach($row as $key => $val)
				{
					$character[$key] = $val ;
				}
				$characters[] = $character;
			}
			 
			
			foreach($characters as $char)
			{
				$p = new Player($char["username"], $char["pos"]);
				$p->set_data($char);
				$ret[] = $p;
			}			
		}catch(Exception $e)
		{
			log_message("error" , "Catch on CharacterList::get()" . $e->getMessage());
		}
    	
		return $ret;
    }

    public function buy($user, $game_id )
    {
    	$ret = array("state" => "error", "desc"=>"unknown");
    	try{
    		$data = array("id"=> $game_id);
    		$qres = $this->query($this->buyQString, $data, $user);
    		
    		if( count($qres) == 1)
    		{
				$ret["state"] = $qres[0]->error === true || $qres[0]->error === "t";
				$ret["desc"] = $qres[0]->error_description ;
				log_message("info", "Buy Field with: " . $ret["state"] . " " . $ret["desc"]);
    		}
    	}catch(Exception $e)
		{
			log_message("error" , "Catch on CharacterList::buy()" . $e->getMessage());
		}
		return $ret;
    }
}
