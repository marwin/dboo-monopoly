#!/usr/bin/env python3
import sys
import os
import uuid
import re
from xml.sax import make_parser, handler

if len(sys.argv) != 2:
    sys.stderr.write('Usage: sys.argv[0] filename\n')
    sys.exit(1)
    
if not os.path.exists(sys.argv[1]):
    sys.stderr.write('ERROR: xml file sys.argv[1] was not found!\n')
    sys.exit(1)

#select create_game ('3926318a-550f-42a1-ba84-373bb5ff1cc4')
#select add_field('3926318a-550f-42a1-ba84-373bb5ff1cc4', 'spassbahnhof', 'railroad');
#select add_buyable_field('3926318a-550f-42a1-ba84-373bb5ff1cc4', 'spassbahnhof', 'railroad', 200, 75);
#select add_street_field('3926318a-550f-42a1-ba84-373bb5ff1cc4', 'suedbahnhof', 'railroad', 200, 75, 'blue', 50, 75, 100, 125, 150, 200);
#select * from get_field_list('3926318a-550f-42a1-ba84-373bb5ff1cc4');

class BuecherHandler(handler.ContentHandler):
	def __init__(self):
		self.tags = set()
		self.uuid = uuid.uuid4()
		self.SQL = "select create_game('" + str(self.uuid) + "');\n"
		self.isName = False
		self.fieldName = ""
		self.isType = False
		self.addNext = False

	def startElement(self, name, attrs):
		if name == "Name":
			self.isName = True
		elif name == "Type":
			self.isType = True
		elif name == "Price" or name == "Mortgage" or name == "Color" or name == "BaseRent" or name == "OneHouseRent" \
			or name == "DoubleHouseRent" or name == "TirpleHouseRent" or name == "QuadrupleHouseRent" or name == "HotelRent":
			self.addNext = True

        
	def characters(self, content):
		text = content.strip()

		if text != "\n":
			if self.isName:
				self.isName = False
				self.fieldName = text

			elif self.isType:
				self.isType = False
				self.SQL += "select "
				if text == "Street":
					self.SQL += "add_street_field('"
				elif text == "Railroad" or text == "Utility":
					self.SQL += "add_buyable_field('"
				else:
					self.SQL += "add_field('"
				self.SQL += str(self.uuid) + "', '" + self.fieldName + "', '" + text + "'"

			elif self.addNext:
				self.addNext = False
				if text.isnumeric():
					self.SQL += ", " + text
				else:
					self.SQL += ",'" + text + "'"
				
	def endElement(self, name):
		if name == "Field":
			self.SQL += ");\n"
		elif name == "Monopoly":
			self.SQL += "select * from get_field_list('" + str(self.uuid) + "');\n"	

	def getSQL(self):
		return self.SQL

filepath = sys.argv[1];

parser = make_parser()
b = BuecherHandler()
parser.setContentHandler(b)
parser.parse(filepath)
print(b.getSQL())