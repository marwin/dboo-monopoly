from datastructures import * 
import plpy

def get_random_numer(min=0, max=1000):
	"""
	Returns a random integer number between @max and @min  [min, max)
	@max is not a part of the range.
	"""
	import random
	return random.randrange(min,max)


def get_two_diced_numbers():
	"""
	Returns two elements as a tuple with random numbers between 1 and 6
	"""
	return ( get_random_numer(1,7), get_random_numer(1,7) )


def get_user( username ):
	sql = plpy.prepare("SELECT * FROM users_p WHERE users_p.name = $1" , ["text"])
	rv = plpy.execute( sql, [username], 1 ) # get only one
	if rv.nrows() == 1 :
		return User( rv[0]  )
		
	return None


if __name__ == "__main__":
	print("Try methods:")


	print("\tSimple dice test:" ) 
	for x in range(10) :
		print("\t\tDices: %d, %d" % get_two_diced_numbers())


	print("\tDice deviation test")
	results = [0,0,0,0,0,0]
	for x in range(1000):
		x,y = get_two_diced_numbers()
		results[x-1] += 1
		results[y-1] += 1
	print("\t\tResult: ", end=" ")
	for x in results :
		print( x, end=" ");
	print()
